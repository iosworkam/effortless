//
//  RequestViewController.swift
//  Effortless Approach
//
//  Created by AMISHA BELADIYA on 29/11/17.
//  Copyright © 2017 Aimaccent. All rights reserved.
//

import UIKit
import PopOverMenu
class RequestViewController: UIViewController,UIAdaptivePresentationControllerDelegate {
  var type = String()
    @IBOutlet weak var nameuser: UILabel!
    @IBOutlet weak var imageview: UIImageView!
    var isStatusGet = Bool()
    @IBOutlet weak var senTfrinedrequest: UIButton!
    @IBOutlet weak var declinestatus: UIButton!
    @IBOutlet weak var acceptfriendreq: UIButton!
     var CuurntUserId = String()
    
      var name = String()
      var Status = String()
    @IBOutlet weak var status: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
          self.getdata()
        self.navigationItem.title = "EFFORTLESS"
       // setupnavigation()
        isStatusGet = false
        observeChildAdded()
        if(type == "UserSetting"){
            declinestatus.isHidden = true
            acceptfriendreq.isHidden = true
            
        }
        else{
            declinestatus.isHidden = false
            acceptfriendreq.isHidden = false
            
        }
        
     getCurruntStatus()
    }
    func observeChildAdded(){
        FirebaseManager.getDatabaseReference().child("Users").observe(.childChanged, with: { (snapshot) in
            self.getCurruntStatus()
        })
    }
    func getCurruntStatus(){
        HUD.show(to: self.view)
        if(CuurntUserId == Common.instance.GetDeviceId()){
            self.acceptfriendreq.isHidden = true
            self.declinestatus.isHidden = true
            self.senTfrinedrequest.isHidden = true
        }
        else{
           
            let userRef = FirebaseManager.getDatabaseReference().child("Friend_req").child(Common.instance.GetDeviceId()).child(CuurntUserId)
            userRef.observeSingleEvent(of: .value, with: { (snapshot) in
                let postDict = snapshot.value as? [String : AnyObject] ?? [:]
             HUD.hide(to: self.view)
                if(postDict.count <= 0){
                    self.getFreind()
                }
                HUD.hide(to: self.view)
                 for postDictvalue in postDict{
                    let dickeyval = postDictvalue.value as! String
                if(dickeyval != nil){
                    if(dickeyval == "sent"){
                        self.isStatusGet = true
                        self.senTfrinedrequest.setTitle("Cancel Friend Request", for: .normal)
                        self.acceptfriendreq.isHidden = true
                        self.declinestatus.isHidden = true
                    }
                    else if(dickeyval  == "received"){
                        self.isStatusGet = true
                        self.senTfrinedrequest.isHidden = true
                        self.acceptfriendreq.isHidden = false
                        self.declinestatus.isHidden = false
                    }
                    }
                    else{
                    self.getFreind()
                    }
                
               
                }
                
            }) { (error) in
                
                 HUD.hide(to: self.view)
                Common.showAlert(with: "Error!", message: error.localizedDescription, for: self)
            }
        }
       
    }

    func getFreind(){
       HUD.show(to: self.view)
            let userRef = FirebaseManager.getDatabaseReference().child("Friends").child(Common.instance.GetDeviceId()).child(CuurntUserId)
            userRef.observeSingleEvent(of: .value, with: { (snapshot) in
                let postDict = snapshot.value as? [String : AnyObject]
                print(postDict ?? "")
                HUD.hide(to: self.view)
                //  for postDictvalue in postDict!{
                
                if(postDict != nil){
                    if((postDict!["date"] as? String) != nil){
                        self.declinestatus.setTitle("Unfriend this User", for: .normal)
                        self.acceptfriendreq.isHidden = true
                        self.declinestatus.isHidden = false
                        self.senTfrinedrequest.isHidden = true
                    }
                    
                }
                else{
                    print("not found")
                    self.senTfrinedrequest.isHidden = false
                    self.acceptfriendreq.isHidden = true
                    self.declinestatus.isHidden = true
                }
                
                
            }) { (error) in
                Common.showAlert(with: "Error!", message: error.localizedDescription, for: self)
            }
        
        
    }
    @IBAction func sentfriendrequset(_ sender: UIButton) {
        if(sender.titleLabel?.text == "Cancel Friend Request"){
            let userDBRef2 = FirebaseManager.getDatabaseReference().child("Friend_req").child(Common.instance.GetDeviceId()).child(CuurntUserId)
            userDBRef2.removeValue()
            let userDBRef =
                FirebaseManager.getDatabaseReference().child("Friend_req").child(CuurntUserId).child(Common.instance.GetDeviceId())
             userDBRef.removeValue()
               self.senTfrinedrequest.setTitle("Send Friend Request", for: .normal)
        }
        else if(sender.titleLabel?.text == "Send Friend Request"){
            HUD.show(to: self.view)
            let uniqueId = Common.instance.GetDeviceId()
           
            
            let postdata:[String : String] = [
                "request_type":"sent"]
           let userDBRef2 = FirebaseManager.getDatabaseReference().child("Friend_req").child(uniqueId).child(CuurntUserId)
           
           
            userDBRef2.setValue(postdata, withCompletionBlock: { (error, ref) in
                if let error_code = error {
                    debugPrint(error_code.localizedDescription)
                    HUD.hide(to: self.view)
                } else  {
                    self.senTfrinedrequest.setTitle("Cancel Friend Request", for: .normal)
                    self.acceptfriendreq.isHidden = true
                    self.declinestatus.isHidden = true
                    self.senTfrinedrequest.isHidden = false
                    
                }
            })
            let postdata2:[String : String] = [
                "request_type":"received"]
            
            let userDBRef =
                FirebaseManager.getDatabaseReference().child("Friend_req").child(CuurntUserId).child(uniqueId)
            
            userDBRef.setValue(postdata2, withCompletionBlock: { (error, ref) in
                if let error_code = error {
                    debugPrint(error_code.localizedDescription)
                    HUD.hide(to: self.view)
                } else  {
                    HUD.hide(to: self.view)
                    
                }
            })
        }
    }
    
    @IBAction func acceptaction(_ sender: UIButton) {
        HUD.show(to: self.view)
        let uniqueId = Common.instance.GetDeviceId()
        
        print(self.printTimestamp())
        let fullDateArr : [String] = self.printTimestamp().components(separatedBy: "at")
        let stringDate = fullDateArr[0] + fullDateArr[1]
           print(stringDate)
        let postdata:[String : String] = [
            "date":stringDate]
        let userDBRef2 = FirebaseManager.getDatabaseReference().child("Friends").child(uniqueId).child(CuurntUserId)
        
    
        userDBRef2.setValue(postdata, withCompletionBlock: { (error, ref) in
            if let error_code = error {
                debugPrint(error_code.localizedDescription)
                HUD.hide(to: self.view)
            } else  {
                self.declinestatus.setTitle("Unfriend this User", for: .normal)
                self.acceptfriendreq.isHidden = true
                self.declinestatus.isHidden = false
                self.senTfrinedrequest.isHidden = true
            }
        })
        let postdata2:[String : String] = [
            "date":stringDate]
        
        let userDBRef =
            FirebaseManager.getDatabaseReference().child("Friends").child(CuurntUserId).child(uniqueId)
        
        userDBRef.updateChildValues(postdata2, withCompletionBlock: { (error, ref) in
            if let error_code = error {
                debugPrint(error_code.localizedDescription)
                HUD.hide(to: self.view)
            } else  {
                HUD.hide(to: self.view)
                
            }
        })
        
        let userDBRefcan = FirebaseManager.getDatabaseReference().child("Friend_req").child(Common.instance.GetDeviceId()).child(CuurntUserId)
        userDBRefcan.removeValue()
        let userDBRefcan2 =
            FirebaseManager.getDatabaseReference().child("Friend_req").child(CuurntUserId).child(Common.instance.GetDeviceId())
        userDBRefcan2.removeValue()
       
    }
    func printTimestamp() -> String {
        let timestamp = DateFormatter.localizedString(from: NSDate() as Date, dateStyle: .medium, timeStyle: .medium)
       
        return timestamp
    }
   
    @IBAction func declinereuest(_ sender: UIButton) {
        let userDBRefcan = FirebaseManager.getDatabaseReference().child("Friends").child(Common.instance.GetDeviceId()).child(CuurntUserId)
        userDBRefcan.removeValue()
        let userDBRefcan2 =
            FirebaseManager.getDatabaseReference().child("Friends").child(CuurntUserId).child(Common.instance.GetDeviceId())
        userDBRefcan2.removeValue()
         self.senTfrinedrequest.setTitle("Send Friend Request", for: .normal)
        self.acceptfriendreq.isHidden = true
        self.declinestatus.isHidden = true
        self.senTfrinedrequest.isHidden = false
    }
    func getdata(){
        let userRef = FirebaseManager.getDatabaseReference().child("Users").child(CuurntUserId)
        userRef.observeSingleEvent(of: .value, with: { (snapshot) in
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            self.imageview.kf.setImage(with: URL(string: (postDict["image"]! as! String)))
            self.nameuser.text = postDict["name"] as? String
            self.status.text = (postDict["status"] as? String)
            if((postDict["image"] as? String) == "default"){
                self.imageview.image = UIImage(named:"person-placeholder")
                
                HUD.hide(to: self.view)
            }
        }) { (error) in
            Common.showAlert(with: "Error!", message: error.localizedDescription, for: self)
             HUD.hide(to: self.view)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setupnavigation(){
        let dormenu = UIBarButtonItem(image: UIImage(named: "dots-vertical"), landscapeImagePhone: nil, style: .done, target: self, action: #selector(slidemenu(_:)))
         self.navigationItem.rightBarButtonItem = dormenu
    }
    @objc func slidemenu(_ sender: UIBarButtonItem) {
        let titles:NSArray = ["Account Setting", "All User", "Log out"]
        
        
        let popOverViewController = PopOverViewController.instantiate()
        popOverViewController.setTitles(titles as! Array<String>)
        
        
        // option parameteres
        // popOverViewController.setSelectRow(1)
        // popOverViewController.setShowsVerticalScrollIndicator(true)
        // popOverViewController.setSeparatorStyle(UITableViewCellSeparatorStyle.singleLine)
        
        popOverViewController.popoverPresentationController?.barButtonItem = sender
        popOverViewController.preferredContentSize = CGSize(width: 200, height:180)
        popOverViewController.presentationController?.delegate = self
        popOverViewController.completionHandler = { selectRow in
            switch (selectRow) {
            case 0:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileSettingViewController") as? ProfileSettingViewController
                
                self.navigationController?.pushViewController(vc!, animated: true)
                break
            case 1:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AllUserListview")
                self.navigationController?.pushViewController(vc!, animated: true)
                break
            case 2:  UserInfo.logOutUser(completion: { (bool) in
                if bool {
                    UserDefaults.standard.removeObject(forKey: "isRegister")
                    let rootVC  = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginbaseView")
                    let navigation = UINavigationController(rootViewController: rootVC)
                    let app = UIApplication.shared.delegate as! AppDelegate
                    app.window?.rootViewController = navigation
                }
            })
                break
            default:
                break
            }
            
        };
        present(popOverViewController, animated: true, completion: nil)
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
        
    }
    @objc func searchaction(_ sender: UIBarButtonItem) {
        // Implement action
    }
}
