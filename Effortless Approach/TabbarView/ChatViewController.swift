//
//  ChatViewController.swift
//  Effortless Approach
//
//  Created by AMISHA BELADIYA on 29/11/17.
//  Copyright © 2017 Aimaccent. All rights reserved.
//

import UIKit
import PopOverMenu
class ChatViewController: UIViewController,UIAdaptivePresentationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
      
      self.navigationItem.title = "EFFORTLESS"
        setupnavigation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setupnavigation(){
        let dormenu = UIBarButtonItem(image: UIImage(named: "dots-vertical"), landscapeImagePhone: nil, style: .done, target: self, action: #selector(slidemenu(_:)))
         self.navigationItem.rightBarButtonItem = dormenu
    }
    @objc func slidemenu(_ sender: UIBarButtonItem) {
        let titles:NSArray = ["Account Setting", "All User", "Log out"]
        
        
        let popOverViewController = PopOverViewController.instantiate()
        popOverViewController.setTitles(titles as! Array<String>)
        
        
        // option parameteres
        // popOverViewController.setSelectRow(1)
        // popOverViewController.setShowsVerticalScrollIndicator(true)
        // popOverViewController.setSeparatorStyle(UITableViewCellSeparatorStyle.singleLine)
        
        popOverViewController.popoverPresentationController?.barButtonItem = sender
        popOverViewController.preferredContentSize = CGSize(width: 200, height:180)
        popOverViewController.presentationController?.delegate = self
        popOverViewController.completionHandler = { selectRow in
            switch (selectRow) {
            case 0:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileSettingViewController") as? ProfileSettingViewController
                
                self.navigationController?.pushViewController(vc!, animated: true)
                break
            case 1:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AllUserListview")
                self.navigationController?.pushViewController(vc!, animated: true)
                break
            case 2:  UserInfo.logOutUser(completion: { (bool) in
                if bool {
                    UserDefaults.standard.removeObject(forKey: "isRegister")
                    let rootVC  = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginbaseView")
                    let navigation = UINavigationController(rootViewController: rootVC)
                    let app = UIApplication.shared.delegate as! AppDelegate
                    app.window?.rootViewController = navigation
                }
            })
                break
            default:
                break
            }
            
        };
        present(popOverViewController, animated: true, completion: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = "Back"
        
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    @objc func searchaction(_ sender: UIBarButtonItem) {
        // Implement action
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
