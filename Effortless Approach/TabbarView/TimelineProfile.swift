//
//  TimelineProfile.swift
//  Effortless Approach
//
//  Created by AMISHA BELADIYA on 29/11/17.
//  Copyright © 2017 Aimaccent. All rights reserved.
//

import UIKit
import GooglePlaces
import FirebaseDatabase
import Firebase
import PopOverMenu
import FBSDKLoginKit
import FBSDKCoreKit
import FirebaseFacebookAuthUI
import GoogleSignIn
class TimelineProfile: UIViewController,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,UIAdaptivePresentationControllerDelegate {
    
       @IBOutlet weak var tablveiwcontrollr: UITableView!
    //location
    let locationManager = CLLocationManager()
    var currentLocation = CLLocation()
    var latitude = Double()
    var longitude = Double()
    var ref: DatabaseReference!
    var UserData = [[String:AnyObject]]()
     var UserDataKey = [String]()
    //MARK: TAbleview delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UserData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell :timelinecell = tableView.dequeueReusableCell(withIdentifier: "timelinecell", for: indexPath) as! timelinecell
        cell.profilepic.setRadius()
        let userdatasingle = self.UserData[indexPath.row]
        
        cell.nametet.text = String(describing: userdatasingle["name"]!)
        cell.timelinetext.text = String(describing: userdatasingle["status"]!)
        if(String(describing: userdatasingle["image"]!) == "default" || String(describing: userdatasingle["image"]!) == ""){
            cell.profilepic.image = UIImage(named:"person-placeholder")
        }
        else{
            if let image = userdatasingle["image"]! as? String{
                cell.profilepic.kf.setImage(with: URL(string: image))
            }
        }
        
       
        if let statusTime = userdatasingle["online"] as? Double{
        
        let timeInterval = Double(statusTime/1000)
        let date = NSDate(timeIntervalSince1970: timeInterval)
        let deltaSeconds = date.timeIntervalSince(Date())
            if(Date.TimeElapsed(seconds: deltaSeconds) == "Just now"){
                cell.onlineimg.image = UIImage(named:"online_icon")
            }
            else{
                //cell.onlineimg.image = UIImage(named:"")
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        _ = self.UserData[indexPath.row]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RequestViewController") as? RequestViewController
        vc?.type = "UserSetting"
        vc?.CuurntUserId = String(UserDataKey[indexPath.row])
        self.navigationController?.pushViewController(vc!, animated: true)
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let postdata:[String : AnyObject] = [
            "online":ServerValue.timestamp()  as AnyObject]
        if(Common.instance.GetDeviceId() != ""){
            let userDBRef = FirebaseManager.getDatabaseReference().child("Users").child(Common.instance.GetDeviceId())
            
            userDBRef.updateChildValues(postdata, withCompletionBlock: { (error, ref) in
                if let error_code = error {
                    debugPrint(error_code.localizedDescription)
                    HUD.hide(to: self.view)
                    
                } else  {
                    HUD.hide(to: self.view)
                    
                    
                }
            })
        }
        HUD.show(to:self.view)
        self.GetLoginUserdata( completion: { (bool, error) in
            if let error = error {
                self.getdata()
            } else {
                Common.showAlert(with: "Alert",message: error?.localizedDescription, for: self)
            }
        })
        
        observeChildAdded()
        
        observeChildAdded()
   
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        if let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore") as? Bool{
        if launchedBefore  {
            print("Not first launch.")
        } else {
            print("First launch, setting UserDefault.")
            UserDefaults.standard.set(true, forKey: "launchedBefore")
        }
    }
        else{
             UserDefaults.standard.set(true, forKey: "launchedBefore")
        }
            // Do any additional setup after loading the view.
    }
    func GetLoginUserdata(completion: @escaping (Bool, Error?) -> Void)  {
        let userDBRef = FirebaseManager.getDatabaseReference().child("Users").child(Common.instance.GetDeviceId())
        userDBRef.observeSingleEvent(of: .value, with: { (snapshot) in
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            print(postDict)
            if let data = postDict as? [String:Any] {
                let userData = User(userData: data)
                let encodedData = NSKeyedArchiver.archivedData(withRootObject: userData)
                UserDefaults.standard.set(encodedData, forKey: "user")
                //  UserDefaults.standard.set(data["key"], forKey: "key")
                
                
            }
        })
    }
    func startLocationManager(){
        //Location Manager Setup
        self.locationManager.delegate = self as CLLocationManagerDelegate
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        latitude = LocationManager.sharedInstance.latitude
       
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        latitude = locValue.latitude
        longitude = locValue.longitude
        
        let postdata:[String : AnyObject] = [
            "lat":String(latitude) as AnyObject ,
            "lng":String(longitude)  as AnyObject,
            "online":ServerValue.timestamp()  as AnyObject]
        if(Common.instance.GetDeviceId() != ""){
        let userDBRef = FirebaseManager.getDatabaseReference().child("Users").child(Common.instance.GetDeviceId())
        
        userDBRef.updateChildValues(postdata, withCompletionBlock: { (error, ref) in
            if let error_code = error {
                debugPrint(error_code.localizedDescription)
                HUD.hide(to: self.view)
                
            } else  {
                HUD.hide(to: self.view)
                
                
            }
        })
    }
    }
  
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupnavigation()
      
      
         self.navigationItem.title = "EFFORTLESS"
        // Do any additional setup after loading the view.
    }
    func observeChildAdded(){
        FirebaseManager.getDatabaseReference().child("Users").observe(.childChanged, with: { (snapshot) in
            self.getdata()
        })
    }
    
    func setupnavigation(){
        let dormenu = UIBarButtonItem(image: UIImage(named: "dots-vertical"), landscapeImagePhone: nil, style: .done, target: self, action: #selector(slidemenu(_:)))
        
        self.navigationItem.rightBarButtonItem = dormenu
    }
    @objc func slidemenu(_ sender: UIBarButtonItem) {
        let titles:NSArray = ["Profile Setting","Account Setting", "All User", "Log out"]
      
        
        let popOverViewController = PopOverViewController.instantiate()
        popOverViewController.setTitles(titles as! Array<String>)
    
        
        // option parameteres
        // popOverViewController.setSelectRow(1)
        // popOverViewController.setShowsVerticalScrollIndicator(true)
        // popOverViewController.setSeparatorStyle(UITableViewCellSeparatorStyle.singleLine)
        
        popOverViewController.popoverPresentationController?.barButtonItem = sender
        popOverViewController.preferredContentSize = CGSize(width: 200, height:180)
        popOverViewController.presentationController?.delegate = self
        popOverViewController.completionHandler = { selectRow in
            switch (selectRow) {
            case 0:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as? RegisterViewController
                vc?.ScreenType = "edit"
                self.navigationController?.pushViewController(vc!, animated: true)
                break
            case 1:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileSettingViewController") as? ProfileSettingViewController
          
            self.navigationController?.pushViewController(vc!, animated: true)
                break
            case 2:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AllUserListview") as? AllUserListview
                vc?.type = "UserSetting"
                self.navigationController?.pushViewController(vc!, animated: true)
                break
            case 3:
                
                    do {
                        
                        GIDSignIn.sharedInstance().signOut()
                        try Auth.auth().signOut()
                        
                      
                    } catch let error {
                       
                    }
                        UserDefaults.standard.removeObject(forKey: "isRegister")
                        let rootVC  = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginbaseView")
                        let navigation = UINavigationController(rootViewController: rootVC)
                        let app = UIApplication.shared.delegate as! AppDelegate
                        app.window?.rootViewController = navigation
                    
               
                
          
                break
            default:
                break
            }
            
        };
        present(popOverViewController, animated: true, completion: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = "Back"
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    @objc func searchaction(_ sender: UIBarButtonItem) {
        // Implement action
    }
    func getdata(){
     
        
        
        let userRef = FirebaseManager.getDatabaseReference().child("Users")
        
        
        userRef.observeSingleEvent(of: .value, with: { (snapshot) in
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            print(postDict)
             HUD.hide(to:self.view)
            self.UserDataKey.removeAll()
            self.UserData.removeAll()
            for dic in postDict{
           
              
                let myLocation = CLLocation(latitude: self.latitude, longitude: self.longitude)
                var dickeyval = dic.value as! [String : AnyObject]
                let lat = Double(dickeyval["lat"] as! String)
                let long = Double(dickeyval["lng"] as! String)
                let myLocation2 = CLLocation(latitude: lat!, longitude:long!)
                let distance = myLocation.distance(from: myLocation2) / 1000
                if(dic.key == Common.instance.GetDeviceId()){
//                    self.UserData.append(dickeyval)
//                    self.UserDataKey.append(dic.key)
                }
                else{
                    self.UserData.append(dickeyval)
                    self.UserDataKey.append(dic.key)
                    if let proximity = dickeyval["proximity"] as? String{
                        let start = proximity.startIndex
                        let end = proximity.index(proximity.endIndex, offsetBy: -1)
                        let substring = proximity[start..<end] // www.stackoverflow
                    
                        var myage = Common.instance.GetAgepreference() as? String
                        
                        
                        if let looking = dickeyval["looking"] as? String,
                            let ageprefe = dickeyval["age"] as? String,
                            myage != ""
                        {
                            
                            if(myage == "45"){
                                myage = "45-100"
                            }
                            else{
                                
                            }
                            var agearray = myage?.split(separator: "-")
                            let miniMumvalue = agearray![0]
                            let maxMumvalue = agearray![1]
                            
                            
//                            if((Common.instance.GetProximity() >= Int(distance))
//                                && (Common.instance.Getlookingfor() == looking.lowercased()) && (miniMumvalue <= ageprefe && maxMumvalue >= ageprefe)){
                            
                                self.UserData.append(dickeyval)
                                self.UserDataKey.append(dic.key)
                          //  }
                            self.tablveiwcontrollr.reloadData()
                        }
                    }

                   

                  print(String(format: "The distance to my buddy is %.01fkm", distance))
              }

            }
            
            if(self.UserData.count>0)
            {
                self.tablveiwcontrollr.reloadData()
            }
            else{
                self.tablveiwcontrollr.isHidden = true
            }
        }) { (error) in
            Common.showAlert(with: "Error!", message: error.localizedDescription, for: self)
        }
    }
    
    /*
    // MARK: - Navigation
     

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
