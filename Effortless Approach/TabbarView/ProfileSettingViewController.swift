//
//  ProfileSettingViewController.swift
//  Effortless Approach
//
//  Created by AMISHA BELADIYA on 02/12/17.
//  Copyright © 2017 Aimaccent. All rights reserved.
//

import UIKit
import Kingfisher
import MBProgressHUD
import Firebase
import FirebaseStorage
class ProfileSettingViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,PECropViewControllerDelegate {
    
    @IBOutlet weak var statusText: UILabel!
    
    @IBOutlet weak var changeSttsu: UIButton!
    @IBOutlet weak var changeimg: UIButton!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var profileimg: UIImageView!
    let picker = UIImagePickerController()
    var type = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        profileimg.setRadius()
         self.getdata()
        self.observeChildAdded()
        self.title = "Profile"
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    @IBAction func chagneimg(_ sender: Any) {
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.allowsEditing = false
        self.present(picker, animated: true, completion: nil)
        picker.delegate = self
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
       
      
        // use the image
        
        var chosenImage : UIImage!
        
        if let img = info[UIImagePickerControllerEditedImage] as? UIImage
        {
            chosenImage = img
            
        }
        else if let img = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            chosenImage = img
        }
        
     dismiss(animated: true, completion: nil)
        self.profileimg.image = chosenImage
        
        let controller = PECropViewController()
        controller.delegate = self
        controller.image = chosenImage
        let navigationController = UINavigationController(rootViewController: controller )
        self.navigationController?.pushViewController(controller, animated: true)

      
        
    }
    @IBAction func changeStatusac(_ sender: Any) {
        let viewController:ChangeStatusView = self.storyboard?.instantiateViewController(withIdentifier: "ChangeStatusView") as! ChangeStatusView 
                  viewController.staustext = statusText.text
          self.navigationController?.pushViewController(viewController, animated: true)
    }
    func cropViewController(_ controller: PECropViewController, didFinishCroppingImage croppedImage: UIImage) {
         HUD.show(to: self.view)
         self.profileimg.image = croppedImage
         self.navigationController?.popViewController(animated: true)
        UserClass.setUserImage(userImg: croppedImage, completion: { (user) in
            self.getdata()
            DispatchQueue.main.async {
              
                HUD.hide(to: self.view)
            }
        }) { (error) in
            debugPrint(error.localizedDescription)
            HUD.hide(to: self.view)
        }

       
    }
    func cropViewControllerDidCancel(_ controller: PECropViewController!) {
        controller.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
   func observeChildAdded(){
     HUD.hide(to: self.view)
    FirebaseManager.getDatabaseReference().child("Users").observe(.childChanged, with: { (snapshot) in
       self.getdata()
    })
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var changeStatusaction: UIButton!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getdata(){
       HUD.show(to: self.view)
          let uniqueId = Common.instance.GetDeviceId()
        let userRef = FirebaseManager.getDatabaseReference().child("Users").child(uniqueId)
        userRef.observeSingleEvent(of: .value, with: { (snapshot) in
            HUD.hide(to: self.view)
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
             print(postDict)
           self.statusText.text = postDict["status"] as? String
            self.username.text = postDict["name"] as? String
            if (postDict["image"] as? String) != nil{
            self.profileimg.kf.setImage(with: URL(string: postDict["image"] as! String))
            if((postDict["image"] as? String) == "default"){
                self.profileimg.image = UIImage(named:"person-placeholder")
            }
            }
        }) { (error) in
            Common.showAlert(with: "Error!", message: error.localizedDescription, for: self)
        }
    }
    

   


}
