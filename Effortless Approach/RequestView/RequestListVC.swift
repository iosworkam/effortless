//
//  RequestListVC.swift
//  Effortless Approach
//
//  Created by AMISHA BELADIYA on 04/12/17.
//  Copyright © 2017 Aimaccent. All rights reserved.
//

import UIKit
import PopOverMenu
class RequestListVC: UIViewController ,UITableViewDataSource,UITableViewDelegate,UIAdaptivePresentationControllerDelegate {
    @IBOutlet weak var nouserfoundView: UIView!
    @IBOutlet weak var usertableview: UITableView!
    var AlldataRequest = [[String:AnyObject]]()
   var UserDataKey = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
      self.navigationItem.title = "EFFORTLESS"
       self.getdatarequest()
        setupnavigation()
       // observeChildAdded()
        // Do any additional setup after loading the view.
    }
    func setupnavigation(){
        let dormenu = UIBarButtonItem(image: UIImage(named: "dots-vertical"), landscapeImagePhone: nil, style: .done, target: self, action: #selector(slidemenu(_:)))
      
        self.navigationItem.rightBarButtonItem = dormenu
    }
    @objc func slidemenu(_ sender: UIBarButtonItem) {
        let titles:NSArray = ["Profile Setting","Account Setting", "All User", "Log out"]
        
        
        let popOverViewController = PopOverViewController.instantiate()
        popOverViewController.setTitles(titles as! Array<String>)
        
        
        // option parameteres
        // popOverViewController.setSelectRow(1)
        // popOverViewController.setShowsVerticalScrollIndicator(true)
        // popOverViewController.setSeparatorStyle(UITableViewCellSeparatorStyle.singleLine)
        
        popOverViewController.popoverPresentationController?.barButtonItem = sender
        popOverViewController.preferredContentSize = CGSize(width: 200, height:180)
        popOverViewController.presentationController?.delegate = self
        popOverViewController.completionHandler = { selectRow in
            switch (selectRow) {
            case 0:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as? RegisterViewController
                vc?.ScreenType = "edit"
                self.navigationController?.pushViewController(vc!, animated: true)
                break
            case 1:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileSettingViewController") as? ProfileSettingViewController
                
                self.navigationController?.pushViewController(vc!, animated: true)
                break
            case 2:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AllUserListview") as? AllUserListview
                vc?.type = "UserSetting"
                self.navigationController?.pushViewController(vc!, animated: true)
                break
            case 3:  UserInfo.logOutUser(completion: { (bool) in
                if bool {
                    UserDefaults.standard.removeObject(forKey: "isRegister")
                    let rootVC  = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginbaseView")
                    let navigation = UINavigationController(rootViewController: rootVC)
                    let app = UIApplication.shared.delegate as! AppDelegate
                    app.window?.rootViewController = navigation
                }
            })
                break
            default:
                break
            }
            
        };
        present(popOverViewController, animated: true, completion: nil)
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
   
    @objc func searchaction(_ sender: UIBarButtonItem) {
        // Implement action
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      self.navigationItem.title = "EFFORTLESS"
      
    }
    func observeChildAdded(){
        FirebaseManager.getDatabaseReference().child("Users").observe(.childChanged, with: { (snapshot) in
            self.getdatarequest()
        })
    }
    func getdatarequest(){
         HUD.show(to: self.view)
        UserDataKey.removeAll()
        AlldataRequest.removeAll()
        let userRef = FirebaseManager.getDatabaseReference().child("Friend_req").child(Common.instance.GetDeviceId())
        userRef.observeSingleEvent(of: .value, with: { (snapshot) in
             HUD.hide(to: self.view)
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            if(postDict.count > 0){
                self.nouserfoundView.isHidden = false
                self.usertableview.isHidden = true
            for dic in postDict{
                let dickeyval = dic.value as! [String:String]
                if((dickeyval["request_type"]) == "received"){
                let userRef = FirebaseManager.getDatabaseReference().child("Users").child(dic.key)
                userRef.observeSingleEvent(of: .value, with: { (snapshot) in
                    let postDict = snapshot.value as? [String : AnyObject] ?? [:]
                    print(postDict)
                    self.AlldataRequest.append(postDict)
                    self.UserDataKey.append(dic.key)
                   
                }) { (error) in
                    
                }
                }
            }
            self.usertableview.reloadData()
            }
            else{
                self.nouserfoundView.isHidden = false
                self.usertableview.isHidden = true
            }
            
        }) { (error) in
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.AlldataRequest.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell :allusercell = tableView.dequeueReusableCell(withIdentifier: "allusercell", for: indexPath) as! allusercell
        cell.userimg?.setRadius()
        let userdatasingle = self.AlldataRequest[indexPath.row] as! [String:AnyObject]
        
        cell.usernam.text = String(describing: userdatasingle["name"]!)
       // cell.status.text = String(describing: userdatasingle["status"]!)
        cell.userimg.kf.setImage(with: URL(string: (userdatasingle["image"])! as! String))
        if(String(describing: userdatasingle["image"]) == "default" || String(describing: userdatasingle["image"]).isEmpty == true){
            cell.userimg.image = UIImage(named:"person-placeholder")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alert = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RequestViewController") as? RequestViewController
        //vc?.type = "UserSetting"
        vc?.CuurntUserId = (UserDataKey[indexPath.row])
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95.0
    }

 

}
