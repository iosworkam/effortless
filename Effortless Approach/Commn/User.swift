//
//  User.swift
//  Taxi
//
//  Created by Bhavin on 17/03/17.
//  Copyright © 2017 icanStudioz. All rights reserved.
//

import UIKit
import FirebaseAuth
let SCREEN_WIDTH = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height

let DEFAULT_BORDER_SIZE: CGFloat = 4.0
let DEFAULT_BORDER_ZERO: CGFloat = 0.0

class User: NSObject, NSCoding {
    
    var device_token: String?
    var image: String?
    var lat: String?
    var lng: String?
    var name: String?
    
    var online: Double?
    var status: String?
    var thumb_image: String?
    
    var agepreference: String?
    var country : String?
    var city: String?
    var proximity: String?
    var looking: String?
    var phone: String?
    var gender: String?
    var age: String?
    
    init(userData:[String:Any]) {
        super.init()
        setData(json: userData as [String : AnyObject])
    }
    
    required init(coder decoder: NSCoder) {
        self.device_token = decoder.decodeObject(forKey: "device_token") as? String ?? ""
        self.image = decoder.decodeObject(forKey: "image") as? String ?? ""
        self.lat   = decoder.decodeObject(forKey: "lat") as? String ?? ""
        self.lng   = decoder.decodeObject(forKey: "lng") as? String ?? ""
        self.name  = decoder.decodeObject(forKey: "name") as? String ?? ""
        self.online = decoder.decodeObject(forKey: "online") as? Double
        self.status = decoder.decodeObject(forKey: "status") as? String ?? ""
        self.thumb_image = decoder.decodeObject(forKey: "thumb_image") as? String ?? ""
        
        self.agepreference    = decoder.decodeObject(forKey: "agepreference") as? String ?? ""
        self.country   = decoder.decodeObject(forKey: "country") as? String ?? ""
        
        self.city = decoder.decodeObject(forKey: "city") as? String ?? ""
        self.proximity = decoder.decodeObject(forKey: "proximity") as? String ?? ""
        self.looking   = decoder.decodeObject(forKey: "looking") as? String ?? ""
        self.phone   = decoder.decodeObject(forKey: "phone") as? String ?? ""
        self.gender    = decoder.decodeObject(forKey: "gender") as? String ?? ""
        self.age    = decoder.decodeObject(forKey: "age") as? String ?? ""
        
        
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(device_token,forKey: "device_token")
        coder.encode(image, forKey: "image")
        coder.encode(lat, forKey: "lat")
        coder.encode(lng, forKey: "lng")
        coder.encode(name, forKey: "name")
        coder.encode(online, forKey: "online")
        coder.encode(status, forKey: "status")
        coder.encode(thumb_image, forKey: "thumb_image")
        
        coder.encode(agepreference, forKey: "agepreference")
        coder.encode(country, forKey: "country")
        coder.encode(city, forKey: "city")
        coder.encode(proximity, forKey: "proximity")
        coder.encode(looking, forKey: "looking")
        coder.encode(phone, forKey: "phone")
        coder.encode(gender,forKey:"gender")
        coder.encode(age, forKey: "age")
        
    }
    
    func setData(json:[String:AnyObject]){
        device_token  = json["device_token"] as? String
        image  = json["image"] as? String
        lat    = json["lat"] as? String
        lng  = json["lng"] as? String
        name   = json["name"] as? String
        online  = json["online"] as? Double
        status = json["status"] as? String
        thumb_image    = json["thumb_image"] as? String
        agepreference     = json["agepreference"] as? String
        country    = json["country"] as? String
        city = json["city"] as? String
        proximity = json["proximity"] as? String
        looking   = json["looking"] as? String
        phone   = json["phone"] as? String
        gender    = json["gender"] as? String
        age = json["age"] as? String
        
    }
}
struct messageStruct{
    
    var from            = String()
    var message         = String()
    var seen            = Bool()
    var time            = Double()
    var type            = String()
    
}

