//
//  Common.swift
//  Taxi
//
//  Created by Bhavin
//  skype : bhavin.bhadani
//

import UIKit

import FirebaseAuth
import Firebase

import MBProgressHUD
import Firebase
import FirebaseStorage
import FirebaseAuth
open class Common {
    
    static let instance = Common()
    public init(){}
    
    class func showAlert(with title:String?, message:String?, for viewController:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(cancelAction)
        viewController.present(alert, animated: true, completion: nil)
    }
    class func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }
    
    func isUserLoggedIn() -> Bool {
        if UserDefaults.standard.data(forKey: "user") != nil{
            return true
        } else {
            return false
        }
    }
    
    func GetDeviceId() -> String{
        if let userid = Auth.auth().currentUser?.uid{
            return userid
        }
        return ""
    }
    func GetProximity() -> Int{
        if let data = UserDefaults.standard.data(forKey: "user"){
            let userData = NSKeyedUnarchiver.unarchiveObject(with: data) as? User
            if( (userData?.proximity) == ""){
                return 0
            }
            let string1 = userData?.proximity
            var substring1 = string1?.substring(0..<3)
            
            return Int(substring1!)!
        } else {
            return 0
        }
    }
    func Getlookingfor() -> String{
        if let data = UserDefaults.standard.data(forKey: "user"){
            let userData = NSKeyedUnarchiver.unarchiveObject(with: data) as? User
            return (userData?.looking?.lowercased())!
        } else {
            return ""
        }
    }
    func GetAgepreference() -> String{
        if let data = UserDefaults.standard.data(forKey: "user"){
            let userData = NSKeyedUnarchiver.unarchiveObject(with: data) as? User
            return (userData?.agepreference)!
        } else {
            return ""
        }
    }
    func GetAge() -> String{
        if let data = UserDefaults.standard.data(forKey: "user"){
            let userData = NSKeyedUnarchiver.unarchiveObject(with: data) as? User
            return (userData?.age)!
        } else {
            return ""
        }
    }
    /*
     func getUserId() -> String {
     if let data = UserDefaults.standard.data(forKey: "user"){
     let userData = NSKeyedUnarchiver.unarchiveObject(with: data) as?
     return (userData?.userId)!
     } else {
     return ""
     }
     //return "3"
     
     }
     func getUserName() -> String {
     if let data = UserDefaults.standard.data(forKey: "user"){
     let userData = NSKeyedUnarchiver.unarchiveObject(with: data) as? User
     return (userData?.first_name)!
     } else {
     return ""
     }
     }
     func getEmail() -> String {
     if let data = UserDefaults.standard.data(forKey: "user"){
     let userData = NSKeyedUnarchiver.unarchiveObject(with: data) as? User
     return (userData?.email)!
     } else {
     return ""
     }
     }
     
     
     
     func getAPIKey() -> String {
     if let key = UserDefaults.standard.value(forKey: "key") as? String{
     return key
     } else {
     return ""
     }
     }
     */
    func removeUserdata() {
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.synchronize()
    }
    
    func getFormattedDate(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateObj = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MMM d, yyyy-HH:mm a"
        return dateFormatter.string(from: dateObj!)
    }
    class func BlueColor() -> UIColor {
        return UIColor(red: 6.0/255.0, green: 169.0/255.0, blue: 244.0/255.0 ,alpha:1.0)
    }
    
}
class setColor: UIColor {
    
    class func BlueColor() -> UIColor {
        return UIColor(red: 6.0/255.0, green: 169.0/255.0, blue: 244.0/255.0 ,alpha:1.0)
    }
    
    class func XXXGreenColor() -> UIColor {
        return UIColor(red: 73/255.0, green: 212/255.0, blue: 86/255.0, alpha: 1.0)
    }
}
class HUD : MBProgressHUD{
    
    class func hide(to view: UIView){
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: view, animated: true)
        }
    }
    
    class func show(to view:UIView) {
        DispatchQueue.main.async {
            MBProgressHUD.showAdded(to: view, animated: true)
        }
    }
}

extension UIImageView {
    func setRadius(radius: CGFloat? = nil) {
        self.layer.cornerRadius = radius ?? self.frame.width / 2;
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 0.5
        self.layer.masksToBounds = true;
    }
}
public extension String {
    
    func substring(_ r: Range<Int>) -> String {
        let fromIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
        let toIndex = self.index(self.startIndex, offsetBy: r.upperBound)
        return self.substring(with: Range<String.Index>(uncheckedBounds: (lower: fromIndex, upper: toIndex)))
    }
    
}
