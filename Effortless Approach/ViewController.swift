//
//  ViewController.swift
//  Effortless Approach
//
//  Created by AMISHA BELADIYA on 27/11/17.
//  Copyright © 2017 Aimaccent. All rights reserved.
//

import UIKit

class ViewController: UIViewController,KIImagePagerDelegate,KIImagePagerDataSource {
    
    @IBOutlet weak var skipbtn: UIButton!
      @IBOutlet weak var mainTitle: UILabel!
    @IBOutlet weak var decs: UILabel!
    @IBOutlet weak var backgroundimg: UIImageView!
    @IBOutlet weak var nextbtn: UIButton!
    var Title  = ["Say Hi","Nearby Strangers","Friendship","Party"]
   var descriptionText  = ["Say Hi to nearest people who are just around you","Make Nearby strangers to your friend!","Friends are easy to find now , just make a move","Share love, Party Hard"]
    var nextIndex = Int()
    var array = [UIImage(named:"bg_screen1"),UIImage(named:"bg_screen2"),UIImage(named:"bg_screen3"),UIImage(named:"bg_screen4")]
   var Midimgarray = [UIImage(named:"one"),UIImage(named:"two"),UIImage(named:"three"),UIImage(named:"four")]
    @IBOutlet weak var imagepager: KIImagePager!
    override func viewDidLoad() {
        super.viewDidLoad()
        nextIndex = 0
        mainTitle.text = Title[0]
        decs.text = descriptionText[0] as? String
        self.backgroundimg.image = Midimgarray[nextIndex]
        self.backgroundimg.setRadius()
        UIApplication.shared.statusBarView?.isHidden = true
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func array(withImages pager: KIImagePager!) -> [Any]! {
        return array
    }
    func contentMode(forImage image: UInt, in pager: KIImagePager!) -> UIViewContentMode {
        return UIViewContentMode.scaleToFill
    }
    func contentMode(forPlaceHolder pager: KIImagePager!) -> UIViewContentMode {
        return UIViewContentMode.scaleToFill
    }
    @IBAction func skipaction(_ sender: Any) {
        let rootVC  = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginbaseView")
        let navigation = UINavigationController(rootViewController: rootVC)
        let app = UIApplication.shared.delegate as! AppDelegate
        app.window?.rootViewController = navigation
    }
    
    @IBAction func goritAction(_ sender: UIButton) {
     
        if(nextbtn.titleLabel?.text == "GOT IT"){
        let rootVC  = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginbaseView")
        let navigation = UINavigationController(rootViewController: rootVC)
        let app = UIApplication.shared.delegate as! AppDelegate
        app.window?.rootViewController = navigation
        }
        else{
             nextIndex = nextIndex+1
            imagepager.setCurrentPage(UInt(nextIndex), animated: true)
            if(nextIndex == array.count - 1){
                 nextbtn.setTitle("GOT IT", for: .normal)
            }
            else{
                nextbtn.setTitle("NEXT", for: .normal)
            }
        }
        mainTitle.text = Title[nextIndex]
        decs.text = descriptionText[nextIndex] as? String
        self.backgroundimg.image = Midimgarray[nextIndex]
    }
    func imagePager(_ imagePager: KIImagePager!, didSelectImageAt index: UInt) {
        
    }
    func imagePager(_ imagePager: KIImagePager!, didScrollTo index: UInt) {
       mainTitle.text = Title[Int(index)]
      decs.text = descriptionText[Int(index)]
    self.backgroundimg.image = Midimgarray[Int(index)]
        if(index == array.count - 1){
            nextbtn.setTitle("GOT IT", for: .normal)
        }
        else{
            nextbtn.setTitle("NEXT", for: .normal)
        }
    }
    
}

