//
//  MYViewController.m
//  MYBlurIntroductionView-Example
//
//  Created by Matthew York on 10/16/13.
//  Copyright (c) 2013 Matthew York. All rights reserved.
//

#import "MYViewController.h"



@interface MYViewController ()

@end

@implementation MYViewController{
    MYBlurIntroductionView *introductionView;
    long pageindex;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarHidden:YES
                                            withAnimation:UIStatusBarAnimationFade];
	// Do any additional setup after loading the view, typically from a nib.
}

-(void)viewDidAppear:(BOOL)animated{
    //Calling this methods builds the intro and adds it to the screen. See below.
    [self buildIntro];
}
-(void)viewWillDisappear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarHidden:NO
                                            withAnimation:UIStatusBarAnimationFade];
    [self buildIntro];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Build MYBlurIntroductionView

-(void)buildIntro{
    //Create Stock Panel with header
 
    //Create Panel From Nib
    MYIntroductionPanel *panel1 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"TestPanel1"];
     MYIntroductionPanel *panel2 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"TestPanel2"];
     MYIntroductionPanel *panel3 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"TestPanel3"];
     MYIntroductionPanel *panel4 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"TestPanel4"];
  
    [panel2.next2 addTarget:self action:@selector(functionName:) forControlEvents:UIControlEventTouchUpInside];
    [panel1.next2 addTarget:self action:@selector(functionName:) forControlEvents:UIControlEventTouchUpInside];
    [panel3.next2 addTarget:self action:@selector(functionName:) forControlEvents:UIControlEventTouchUpInside];
    [panel4.gotitButton addTarget:self action:@selector(gotit:) forControlEvents:UIControlEventTouchUpInside];
    
    [panel2.skipbtn addTarget:self action:@selector(gotit:) forControlEvents:UIControlEventTouchUpInside];
    [panel1.skipbtn addTarget:self action:@selector(gotit:) forControlEvents:UIControlEventTouchUpInside];
    [panel3.skipbtn addTarget:self action:@selector(gotit:) forControlEvents:UIControlEventTouchUpInside];
    
     panel1.centerImg.layer.cornerRadius = panel1.centerImg.frame.size.height /2;
     panel1.centerImg.layer.masksToBounds = YES;
     panel1.centerImg.layer.borderWidth = 0;
    
    panel2.centerImg.layer.cornerRadius = panel2.centerImg.frame.size.height /2;
    panel2.centerImg.layer.masksToBounds = YES;
    panel2.centerImg.layer.borderWidth = 0;
    
    panel3.centerImg.layer.cornerRadius = panel3.centerImg.frame.size.height /2;
    panel3.centerImg.layer.masksToBounds = YES;
    panel3.centerImg.layer.borderWidth = 0;
    
    panel4.centerImg.layer.cornerRadius = panel4.centerImg.frame.size.height /2;
    panel4.centerImg.layer.masksToBounds = YES;
    panel4.centerImg.layer.borderWidth = 0;
//Add panels to an array

    NSArray *panels = @[panel1, panel2, panel3, panel4];
    
    //Create the introduction view and set its delegate
     introductionView = [[MYBlurIntroductionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    introductionView.delegate = self;
   
    [introductionView setBackgroundColor:[UIColor colorWithRed:90.0f/255.0f green:175.0f/255.0f blue:113.0f/255.0f alpha:0.65]];
    //introductionView.LanguageDirection = MYLanguageDirectionRightToLeft;
   
    //Build the introduction with desired panels
    [introductionView buildIntroductionWithPanels:panels];
    
    //Add the introduction to your view
    [self.view addSubview:introductionView];
}
- (void) functionName:(UIButton *) sender {
    
    [introductionView changeToPanelAtIndex:pageindex + 1];
    NSLog(@"Tag : hello");
    
}
- (void) gotit:(UIButton *) sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"LoginbaseView"];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
    [[UIApplication sharedApplication].keyWindow setRootViewController:navigationController];
   
    
}
#pragma mark - MYIntroduction Delegate 

-(void)introduction:(MYBlurIntroductionView *)introductionView didChangeToPanel:(MYIntroductionPanel *)panel withIndex:(NSInteger)panelIndex{
    NSLog(@"Introduction did change to panel %ld", (long)panelIndex);
    pageindex = panelIndex;
    if(pageindex == 3){
      
    }
   // [introductionView changeToPanelAtIndex:3];
  
}

-(void)introduction:(MYBlurIntroductionView *)introductionView didFinishWithType:(MYFinishType)finishType {
    NSLog(@"Introduction did finish");
    
}
- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
