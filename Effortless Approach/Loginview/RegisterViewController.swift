//
//  RegisterViewController.swift
//  Effortless Approach
//
//  Created by AMISHA BELADIYA on 27/11/17.
//  Copyright © 2017 Aimaccent. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import FirebaseAuth
import GooglePlaces
import FirebaseCore
import Firebase
import FirebaseDatabase
class RegisterViewController: UIViewController,CLLocationManagerDelegate {

    @IBOutlet weak var passwordTopconstrain: NSLayoutConstraint!
    @IBOutlet weak var emailTopcontrain: NSLayoutConstraint!
    @IBOutlet weak var passwordextHeightcontsrtain: NSLayoutConstraint!
    @IBOutlet weak var emailtextHeightcontsrtain: NSLayoutConstraint!
    var isValid = Bool()
    let locationManager = CLLocationManager()
    var currentLocation = CLLocation()
    var latitude = Double()
    var longitude = Double()
    var gender = String()
    var Distance = String()
    var LookingFor = String()
     var Ageperfre = String()
    
    var AgeData = NSArray()
    var ScreenType = String()
    
    var username = String()
    @IBOutlet weak var Submitbtn: UIButton!
    @IBOutlet weak var age1825: UIButton!
    @IBOutlet weak var age2535: UIButton!
    @IBOutlet weak var age45: UIButton!
    @IBOutlet weak var age3545: UIButton!
    @IBOutlet weak var secondTandc: UIButton!
    @IBOutlet weak var firstTandc: UIButton!
    @IBOutlet weak var country: SkyFloatingLabelTextField!
    @IBOutlet weak var agetetx: SkyFloatingLabelTextField!
    @IBOutlet weak var phonenumber: SkyFloatingLabelTextField!
    @IBOutlet weak var city: SkyFloatingLabelTextField!
    var USerStandardValue = [String:AnyObject]()
    @IBOutlet weak var female: UIButton!
    @IBOutlet weak var male: UIButton!
    @IBOutlet weak var displayname: SkyFloatingLabelTextField!
    @IBOutlet weak var password: SkyFloatingLabelTextField!
    @IBOutlet weak var emailtext: SkyFloatingLabelTextField!
    
    @IBOutlet weak var scrollView: UIScrollView!
    var timercall = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false;
        self.scrollView.contentInset = UIEdgeInsets.zero
          self.locationManager.requestWhenInUseAuthorization()

        if(ScreenType == "edit"){
             self.setConstarin()
            city.becomeFirstResponder()
              Submitbtn.setTitle("UPDATE DETAILS", for: .normal)
              self.navigationItem.title = "Account Settings"
              self.GetLoginUserdata( completion: { (bool, error) in
                if error != nil {
                    
                } else {
                    Common.showAlert(with: "Alert",message: error?.localizedDescription, for: self)
                }
            })
        }
       else if(ScreenType == "register"){
              self.setConstarin()
              self.navigationItem.title = "Register"
             displayname.text = username
             city.becomeFirstResponder()
            
        }
            
        else{
              self.navigationItem.title = "Register"
        }
        self.hideKeyboardWhenTappedAround()
        self.dismissKeyboard()
        timercall =   Timer.scheduledTimer(timeInterval: 0.3,
                                           target: self,
                                           selector: #selector(self.cheakValueref),
                                           userInfo: nil,
                                           repeats: true)
    }
    func setConstarin(){
        password.isHidden = true
        emailtext.isHidden = true
        passwordTopconstrain.constant = 0
        emailTopcontrain.constant = 0
        emailtextHeightcontsrtain.constant = 0
        passwordextHeightcontsrtain.constant = 0
    }
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
      @objc func cheakValueref(){
        if(emailtext.text?.isEmpty == true){
            if(ScreenType == "edit" || ScreenType == "register"){
            }
            else{
            
                isValid = false
            }
            
        }
        if(password.text?.isEmpty == true){
            if(ScreenType == "edit" || ScreenType == "register"){
            }
            else{
               
                isValid = false
            }
            
        }
        if(displayname.text?.isEmpty == true){
           
            isValid = false
        }
        else if(city.text?.isEmpty == true){
         
            isValid = false
        }
        else if(country.text?.isEmpty == true){
           
            isValid = false
        }
        else if(phonenumber.text?.isEmpty == true){
         
            isValid = false
        }
            
        else if(agetetx.text?.isEmpty == true){
         
            isValid = false
        }
        else if(firstTandc.isSelected == true || secondTandc.isSelected == true){
           
            isValid = false
        }
        else if(Distance.isEmpty == true){
            
            isValid = false
        }
        else if(LookingFor.isEmpty == true || gender.isEmpty == true || Ageperfre.isEmpty == true ){
            
            isValid = false
        }
        else {
            timercall.invalidate()
            Submitbtn.backgroundColor = UIColor(red: 7/255, green: 84/255, blue: 94/255, alpha: 1.0)
            isValid = true
        }
    }
   func cheakNull() -> Bool{
         if(emailtext.text?.isEmpty == true){
            if(ScreenType == "edit" || ScreenType == "register"){
            }
            else{
                Common.showAlert(with: "Alert", message: "Enter your Email", for: self)
                isValid = false
            }
            
        }
        if(password.text?.isEmpty == true){
            if(ScreenType == "edit" || ScreenType == "register"){
            }
            else{
                Common.showAlert(with: "Alert", message: "Enter your Email", for: self)
                isValid = false
            }
            
        }
        if(displayname.text?.isEmpty == true){
            Common.showAlert(with: "Alert", message: "Enter your Name", for: self)
            isValid = false
        }
        else if(city.text?.isEmpty == true){
            Common.showAlert(with: "Alert", message: "Enter your City", for: self)
            isValid = false
        }
        else if(country.text?.isEmpty == true){
            Common.showAlert(with: "Alert", message: "Enter your Country", for: self)
            isValid = false
        }
        else if(phonenumber.text?.isEmpty == true){
            Common.showAlert(with: "Alert", message: "Enter your Mobile Number", for: self)
            isValid = false
        }
            
        else if(agetetx.text?.isEmpty == true){
            Common.showAlert(with: "Alert", message: "Enter your Age", for: self)
            isValid = false
        }
        else if(Distance.isEmpty == true){
            Common.showAlert(with: "Alert", message: "Please Select the distance", for: self)
            isValid = false
        }
        else if(firstTandc.isSelected == true || secondTandc.isSelected == true){
            Common.showAlert(with: "Alert", message: "Please accepet the Terms & Condition", for: self)
            isValid = false
        }
      
        else {
            timercall.invalidate()
            Submitbtn.backgroundColor = UIColor(red: 7/255, green: 84/255, blue: 94/255, alpha: 1.0)
            isValid = true
        }
        return isValid
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        latitude = locValue.latitude
        longitude = locValue.longitude
    }
    func startLocationManager(){
        //Location Manager Setup
        self.locationManager.delegate = self as CLLocationManagerDelegate
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        latitude = LocationManager.sharedInstance.latitude
    }
    @IBAction func loginaction(_ sender: Any) {
        USerStandardValue = ["username":displayname.text as AnyObject,"city":city.text as AnyObject,"country":country.text as AnyObject,"Authage":agetetx.text as AnyObject,"gender" : gender as AnyObject ,"distance":Distance as AnyObject,"lookinfor":LookingFor as AnyObject,"age":Ageperfre as AnyObject]
        print(USerStandardValue)
       UserDefaults.standard.set(USerStandardValue, forKey: "UserData")
        print(UserDefaults.standard.value(forKey: "UserData") ?? "")
        if(cheakNull()){
            if(ScreenType == "edit" || ScreenType == "register"){
                self.createRecent( completion: { (bool, error) in
                    if let error = error {
                        debugPrint(error.localizedDescription)
                    } else {
                        Common.showAlert(with: "Alert",message: error?.localizedDescription, for: self)
                    }
                })
            }
            else{
            Auth.auth().createUser(withEmail: emailtext.text!, password: password.text!) { (user, error) in
                if let error = error {
                    Common.showAlert(with: "Alert",message: error.localizedDescription, for: self)
                    return
                }
                else{
                    do {
                         UserDefaults.standard.set(true, forKey: "isRegister")
                        self.createRecent( completion: { (bool, error) in
                            if let error = error {
                                debugPrint(error.localizedDescription)
                            } else {
                                Common.showAlert(with: "Alert",message: error?.localizedDescription, for: self)
                            }
                        })
                        let testController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = testController
                    } catch let signOutError as NSError {
                        print ("Error signing out: %@", signOutError)
                    }
                }
            }
        }
        }
        
    }
    //MARK:Male/female is selected
    @IBAction func CheakBoxSelected(_ sender: UIButton) {
       
        if(sender.isSelected == false){
            sender.isSelected = true
             gender  = "male"
        }
        else{
            sender.isSelected = false
             gender  = ""
        }
        female.isSelected = false
    }
    @IBAction func FemaleSlected(_ sender: UIButton) {
        
        if(sender.isSelected == false){
            sender.isSelected = true
            gender  = "female"
        }
        else{
            sender.isSelected = false
          gender  = ""
        }
        male.isSelected = false
        
    }
    
    @IBOutlet weak var Lookinfor_female: UIButton!
    @IBOutlet weak var Lookinfor_male: UIButton!
    @IBOutlet weak var Lookinfor_Either: UIButton!
    
    //MARK:looking Male/female is selected
    @IBAction func LookingformaleBoxSelected(_ sender: UIButton) {
        
        if(sender.isSelected == false){
            sender.isSelected = true
            LookingFor  = "male"
        }
        else{
            sender.isSelected = false
             LookingFor  = ""
        }
        Lookinfor_female.isSelected = false
        Lookinfor_Either.isSelected = false
        
    }
    @IBAction func LookingforFemaleSlected(_ sender: UIButton) {
        
        if(sender.isSelected == false){
            sender.isSelected = true
          LookingFor  = "female"
        }
        else{
            sender.isSelected = false
             LookingFor  = ""
        }
        Lookinfor_male.isSelected = false
        Lookinfor_Either.isSelected = false
        
    }
    @IBAction func LookingforEitherSlected(_ sender: UIButton) {
        
        if(sender.isSelected == false){
            sender.isSelected = true
           LookingFor  = "either"
        }
        else{
            sender.isSelected = false
             LookingFor  = ""
        }
     
        Lookinfor_male.isSelected = false
         Lookinfor_female.isSelected = false
    }
    //MARK:AGE Cheak
   
    @IBAction func agebetween1825(_ sender: UIButton) {
        if(sender.isSelected == false){
            sender.isSelected = true
           
            Ageperfre = "18-25"
        }
        else{
            
            sender.isSelected = false
        }
    }
    @IBAction func agebetween2535(_ sender: UIButton) {
        if(sender.isSelected == false){
            sender.isSelected = true
            Ageperfre = "25-35"
           
        }
        else{
            
            sender.isSelected = false
        }
    }
    @IBAction func agebetween3545(_ sender: UIButton) {
        if(sender.isSelected == false){
            sender.isSelected = true
            Ageperfre = "35-45"
           
        }
        else{
           
            sender.isSelected = false
        }
    }
    @IBAction func agebetween45above(_ sender: UIButton) {
        if(sender.isSelected == false){
            sender.isSelected = true
            Ageperfre = "45"
            
        }
        else{
         
            sender.isSelected = false
        }
    }
    //MARK:DISTANCE mark
    
    @IBOutlet weak var Distance100: UIButton!
    @IBOutlet weak var Distance200: UIButton!
    @IBOutlet weak var Distance300: UIButton!
    
    @IBAction func Distance100(_ sender: UIButton) {
        if(sender.isSelected == false){
            sender.isSelected = true
            Distance  = "100m"
        }
        else{
            sender.isSelected = false
             Distance  = ""
        }
        Distance200.isSelected = false
        Distance300.isSelected = false
    }
    @IBAction func Distance200(_ sender: UIButton) {
        if(sender.isSelected == false){
            sender.isSelected = true
             Distance  = "200m"
        }
        else{
            sender.isSelected = false
             Distance  = ""
        }
        Distance100.isSelected = false
        Distance300.isSelected = false
    }
    @IBAction func Distance300(_ sender: UIButton) {
        if(sender.isSelected == false){
            sender.isSelected = true
             Distance  = "200+m"
        }
        else{
            sender.isSelected = false
             Distance  = ""
        }
           Distance200.isSelected = false
           Distance100.isSelected = false
    }
    @IBAction func FirstTandC(_ sender: UIButton) {
        if(sender.isSelected == false){
            sender.isSelected = true
        }
        else{
            sender.isSelected = false
        }
    }
    @IBAction func secondTandC(_ sender: UIButton) {
        if(sender.isSelected == false){
            sender.isSelected = true
        }
        else{
            sender.isSelected = false
        }
    }
   
    //MARK:Distance MArk
    func createRecent(completion: @escaping (Bool, Error?) -> Void)  {
        
        
        let uniqueId = Common.instance.GetDeviceId()
        print("Your device identifires =>\(String(describing: uniqueId))")
        let postdata = [
            "device_token":"\(uniqueId)",
            "image":"default",
            "lat":String(self.latitude),
            "lng":String(self.longitude),
            "name":displayname.text!,
            "online":ServerValue.timestamp() ,
            "status":"Hi there I'm using Effortless Approch",
            "thumb_image":"default" ,
            "agepreference":Ageperfre,
            "city":city.text ?? "",
            "country":country.text ?? "",
            "proximity":"\(Distance)",
            "looking" : "\(LookingFor)",
            "phone":phonenumber.text ?? "",
            "gender":"\(gender)",
            "age": agetetx.text] as [String : Any]
        
         HUD.show(to: self.view)
        
         if(ScreenType == "edit"){
            let userDBRef = FirebaseManager.getDatabaseReference().child("Users").child(uniqueId)
            userDBRef.updateChildValues(postdata, withCompletionBlock: { (error, ref) in
                if let error_code = error {
                    debugPrint(error_code.localizedDescription)
                    HUD.hide(to: self.view)
                } else  {
                    let userData = User(userData: postdata)
                    let encodedData = NSKeyedArchiver.archivedData(withRootObject: userData)
                
                    UserDefaults.standard.set(encodedData, forKey: "user")
                     HUD.hide(to: self.view)
                     Common.showAlert(with: "Success!",message:"Update profile succesfully", for: self)
                   
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                    for aViewController in viewControllers {
                        if aViewController is TimelineProfile {
                            self.navigationController!.popToViewController(aViewController, animated: true)
                        }
                    }
                    
//                    let testController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
//                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                    appDelegate.window?.rootViewController = testController
//                    HUD.hide(to: self.view)
                    
                }
            })
        }
        else{
       
        
      
        // if(ScreenType == "edit"){
        let userDBRef = FirebaseManager.getDatabaseReference().child("Users").child(uniqueId)
        userDBRef.setValue(postdata, withCompletionBlock: { (error, ref) in
            if let error_code = error {
                debugPrint(error_code.localizedDescription)
                HUD.hide(to: self.view)
               
            } else  {
             
                HUD.hide(to: self.view)
                let userData = User(userData: postdata)
                let encodedData = NSKeyedArchiver.archivedData(withRootObject: userData)
                UserDefaults.standard.set(encodedData, forKey: "user")
                let testController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = testController
                HUD.hide(to: self.view)
               
            }
        })
      
        
           
        }
}
func GetLoginUserdata(completion: @escaping (Bool, Error?) -> Void)  {
    let userDBRef = FirebaseManager.getDatabaseReference().child("Users").child(Common.instance.GetDeviceId())
    userDBRef.observeSingleEvent(of: .value, with: { (snapshot) in
        let postDict = snapshot.value as? [String : AnyObject] ?? [:]
        print(postDict)
        if let data = postDict as? [String:Any] {
            let userData = User(userData: data)
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: userData)
            UserDefaults.standard.set(encodedData, forKey: "user")
            //  UserDefaults.standard.set(data["key"], forKey: "key")
            
            self.displayname.text = userData.name
            self.city.text = userData.city
            self.country.text = userData.country
            self.agetetx.text = userData.age
             self.phonenumber.text = userData.phone
            if let Distance =  userData.proximity{
                self.Distance = Distance
                if(Distance == "100m")
                {
                    self.Distance100.isSelected = true
                }
                else if(Distance == "200m"){
                    self.Distance200.isSelected = true
                }
                else {
                    self.Distance300.isSelected = true
                }
            }
            
            
            if let gender =  userData.gender{
                self.gender = gender
                if(gender.lowercased() == "male")
                {
                    self.male.isSelected = true
                }
                else if(gender.lowercased() == "female"){
                    self.female.isSelected = true
                }
            }
            if let Looking_for =  userData.looking{
                self.LookingFor = Looking_for
                if(self.LookingFor.lowercased() == "male")
                {
                    self.Lookinfor_male.isSelected = true
                }
                else if(self.LookingFor.lowercased() == "female"){
                    self.Lookinfor_female.isSelected = true
                }
                else if(self.LookingFor.lowercased() == "either"){
                    self.Lookinfor_Either.isSelected = true
                }
            }
            if let age =  userData.agepreference{
                
                if age == "18-25"{
                    self.Ageperfre = "18-25"
                    self.age1825.isSelected = true
                }
                if age == "26-35"{
                    self.Ageperfre = "25-35"
                    self.age2535.isSelected = true
                    
                }
                if age == "36-45"{
                    self.Ageperfre = "35-45"
                    self.age3545.isSelected = true
                }
                if age == "45+"{
                    self.Ageperfre = "45+"
                    self.age45.isSelected = true
                }
            }
            
            
            
        }
    })
}
}
