//
//  LoginViewController.swift
//  Effortless Approach
//
//  Created by AMISHA BELADIYA on 27/11/17.
//  Copyright © 2017 Aimaccent. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import GooglePlaces
import FirebaseCore
import Firebase
import FirebaseAuth
import FirebaseDatabase
class LoginViewController: UIViewController,CLLocationManagerDelegate {
    //location
    let locationManager = CLLocationManager()
    var currentLocation = CLLocation()
    var latitude = Double()
    var longitude = Double()
    var isValid = Bool()
    var ref: DatabaseReference!
    @IBOutlet weak var password: SkyFloatingLabelTextField!
    @IBOutlet weak var emailtext: SkyFloatingLabelTextField!
    override func viewDidLoad() {
        
        super.viewDidLoad()
     //   UIApplication.shared.statusBarView?.isHidden = false
     
        
      
        self.locationManager.requestAlwaysAuthorization()
        self.title = "Login"
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
      
        // Do any additional setup after loading the view.
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        latitude = locValue.latitude
        longitude = locValue.longitude
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func cheakNull() -> Bool{
        if(password.text?.isEmpty == true){
            Common.showAlert(with: "Alert", message: "Enter your Email", for: self)
            isValid = false
        }
        else if(emailtext.text?.isEmpty == true){
            Common.showAlert(with: "Alert", message: "Enter your password", for: self)
            isValid = false
        }
        else {
            isValid = true
        }
        return isValid
    }
   
    @IBAction func loginaction(_ sender: Any) {
       
        
        if(cheakNull()){
            
            HUD.show(to: self.view)
            Auth.auth().signIn(withEmail: emailtext.text!, password: password.text!) { (user, error) in
                if let error = error {
                    
                    HUD.hide(to: self.view)
                  
                     Common.showAlert(with: "Alert",message: error.localizedDescription, for: self)
                    return
                }
                else{
                    print("sucuess")
                  HUD.hide(to: self.view)
                    UserDefaults.standard.set(true, forKey: "isRegister")
                    if let user = user {
                      /*  let postdata:[String : String] = [
                            "lat":String(self.latitude) as String,
                            "lng":String(self.longitude)]
                        
                        let userDBRef = FirebaseManager.getDatabaseReference().child("Users").child(Common.instance.GetDeviceId())
                        
                        userDBRef.updateChildValues(postdata, withCompletionBlock: { (error, ref) in
                            if let error_code = error {
                                debugPrint(error_code.localizedDescription)
                               
                                
                            } else  {
                                HUD.hide(to: self.view)
                               
                                
                            }
                        })
                        */
                        do {
                            let testController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = testController
                        } catch let signOutError as NSError {
                            print ("Error signing out: %@", signOutError)
                        }
                    }
                }
            }
        }
 

       
    }
     func createRecent(completion: @escaping (Bool, Error?) -> Void)  {
         let uniqueId = Common.instance.GetDeviceId()
       
        
        let postdata:[String : String] = [
            "device_token:":(uniqueId),
            "image":"",
            "lat":String(self.latitude) as String,
            "lng":String(self.longitude),
            "name":"",
            "online":"",
            "status":"Hi there I'm using Effortless Approch",
            "thumb_image":""]
        
        
        self.ref = Database.database().reference()
        self.ref.child("Users").child(uniqueId)
        self.ref.setValue(postdata, withCompletionBlock: { (error, ref) in
            if error != nil {
                completion(false, error)
            }
            else  {
                completion(true, nil)
            }
        })
    }
    //MARK:location
   
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
