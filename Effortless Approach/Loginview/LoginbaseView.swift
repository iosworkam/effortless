//
//  LoginbaseView.swift
//  Effortless Approach
//
//  Created by AMISHA BELADIYA on 01/12/17.
//  Copyright © 2017 Aimaccent. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import FBSDKCoreKit
import FirebaseFacebookAuthUI
import GoogleSignIn
import GooglePlaces
class LoginbaseView: UIViewController,GIDSignInDelegate,GIDSignInUIDelegate,CLLocationManagerDelegate {
   
    let locationManager = CLLocationManager()
    var currentLocation = CLLocation()
    var latitude = Double()
    var longitude = Double()
    var fullName : String?
    var profileurl : String?
    
 @IBOutlet weak var signInButton: GIDSignInButton!

    @IBOutlet var fbSignInBtn: UIButton!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().uiDelegate = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
      //MARK-------------------------------------------------login with facebook delegate------------------------------------------------
    @IBAction func loginWithFacebook(_ sender: Any) {
        let loginManager = FBSDKLoginManager()
          HUD.show(to: self.view)
        loginManager.logIn(withReadPermissions: ["public_profile", "email", "user_friends"], from: self, handler:  { (loginResult,error)  in
            print(loginResult)
            if(error == nil){
            let params = ["fields": "email, name, gender, picture"]
            
            FBSDKGraphRequest(graphPath: "me", parameters: params).start {(connection, result, error) -> Void in
                  HUD.hide(to: self.view)
                if error != nil {
                    NSLog(error.debugDescription)
                    return
                }
                else
                {
                    if let result = result as? [String:AnyObject],
                        let email: String = result["email"] as? String,
                        let fbId: String = result["id"] as? String,
                        let name: String = result["name"] as? String,
                        let picture: NSDictionary = result["picture"] as? NSDictionary,
                        let data: NSDictionary = picture["data"] as? NSDictionary,
                        let url: String = data["url"] as? String {
                        
                        print("Email: \(email)")
                        print("fbID: \(fbId)")
                        print("Name: \(name)")
                        print("URL Picture: \(url)")
                      
                        self.fullName = name as String
                        self.profileurl = url
                        
                    }
                    if let kayval = FBSDKAccessToken.current().tokenString{
                        let credential = FacebookAuthProvider.credential(withAccessToken: kayval)
                        
                        
                      
                        Auth.auth().signIn(with: credential) { (user, error) in
                            
                            
                            if error != nil {
                                // ...
                                return
                            }
                            else{
                                
                                
                                self.GetLoginUserdata( completion: { (bool, error) in
                                    if error != nil {
                                        
                                    } else {
                                        Common.showAlert(with: "Alert",message: error?.localizedDescription, for: self)
                                    }
                                })
                                
                              
                                
                            }
                        }
                    }
                    else {
                        print("Error.....")
                    }
                    
                }
            }
            }
        })
    
    }
  
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "register"{
            let nextScene = segue.destination as? RegisterViewController
            nextScene?.ScreenType = "localregister"
        }
    }
    //MARK-------------------------------------------------login with google and It's Delegate delegate------------------------------------------------
    @IBAction func googleplsuaction(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
        GIDSignIn.sharedInstance().delegate = self
    }
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
                       withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
          /*  let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
         
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
          */
              HUD.show(to: self.view)
            print(user)
            fullName = user.profile.name
            let dimension = round(100 * UIScreen.main.scale)
            profileurl = user.profile.imageURL(withDimension: UInt(dimension)).absoluteString
            guard let authentication = user.authentication else { return }
            let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                           accessToken: authentication.accessToken)
            Auth.auth().signIn(with: credential) { (user, error) in
                  HUD.hide(to: self.view)
                if error != nil {
                    // ...
                    return
                }
                else{
                    
                    self.GetLoginUserdata( completion: { (bool, error) in
                        if error != nil {
                            
                        } else {
                            Common.showAlert(with: "Alert",message: error?.localizedDescription, for: self)
                        }
                    })
                    
                    
                    
                    
                }
            }
        }
        else {
            print("Error.....")
        }
    }
    func GetLoginUserdata(completion: @escaping (Bool, Error?) -> Void)  {
        let userDBRef = FirebaseManager.getDatabaseReference().child("Users").child(Common.instance.GetDeviceId())
        userDBRef.observeSingleEvent(of: .value, with: { (snapshot) in
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            print(postDict)
            if let data = postDict as? [String:Any] {
                let userData = User(userData: data)
                let encodedData = NSKeyedArchiver.archivedData(withRootObject: userData)
                UserDefaults.standard.set(encodedData, forKey: "user")
                //  UserDefaults.standard.set(data["key"], forKey: "key")
                
               if(Auth.auth().currentUser != nil){
                    
               if postDict.count > 9 {
                    print("sign in")
                    let testController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = testController
                } else {
                    do {
                       
                            //UserDefaults.standard.set(true, forKey: "isRegister")
                            self.createRecent( completion: { (bool, error) in
                                if let error = error {
                                    debugPrint(error.localizedDescription)
                                } else {
                                    Common.showAlert(with: "Alert",message: error?.localizedDescription, for: self)
                                }
                            })
                            
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
                            vc.ScreenType = "register"
                            vc.username = self.fullName!
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                        
                    }
                }
            }
            }
            
            
        })
    }
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
    }
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }

    

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        latitude = locValue.latitude
        longitude = locValue.longitude
    }
    func startLocationManager(){
        //Location Manager Setup
        self.locationManager.delegate = self as CLLocationManagerDelegate
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        latitude = LocationManager.sharedInstance.latitude
    }
    func createRecent(completion: @escaping (Bool, Error?) -> Void)  {
        
        HUD.show(to: self.view)
        let uniqueId = Common.instance.GetDeviceId()
        print("Your device identifires =>\(String(describing: uniqueId))")
        let postdata = [
            "device_token":"\(uniqueId)",
            "image":profileurl,
            "lat":String(self.latitude),
            "lng":String(self.longitude),
            "name":fullName ?? "",
            "online":ServerValue.timestamp() ,
            "status":"Hi there I'm using Effortless Approch",
            "thumb_image":"default"] as [String : Any]
        
        
        // if(ScreenType == "edit"){
        let userDBRef = FirebaseManager.getDatabaseReference().child("Users").child(uniqueId)
        userDBRef.setValue(postdata, withCompletionBlock: { (error, ref) in
            if let error_code = error {
                debugPrint(error_code.localizedDescription)
                HUD.hide(to: self.view)
                
            } else  {
                
                HUD.hide(to: self.view)
                
            }
        })
        
    }/*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
