//
//  TabbarViewController.swift
//  Effortless Approach
//
//  Created by AMISHA BELADIYA on 28/11/17.
//  Copyright © 2017 Aimaccent. All rights reserved.
//

import UIKit
import FirebaseAuth
class TabbarViewController: UITabBarController {

    @IBOutlet weak var tabbar: UITabBar!
    override func viewDidLoad() {
        super.viewDidLoad()
       // print(Auth.auth().currentUser?.email)
      
        let aTabArray: [UITabBarItem] = tabbar.items!
        
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .normal)
         UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.yellow], for: .selected)
        //UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: selectedColor], for: .selected)
        for item in aTabArray {
            
            item.image = item.image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
         
            
            
        }
        
       // self.tabbar.tintColor = UIColor.white
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
