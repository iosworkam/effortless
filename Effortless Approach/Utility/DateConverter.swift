//
//  DateConverter.swift
//  storychat
//
//  Created by Priyesh Marvi on 15/04/17.
//  Copyright © 2017 Priyesh Marvi. All rights reserved.
//

import Foundation

extension Date {
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
    
    static func Date2String(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        return formatter.string(from:date)
    }
    
    static func String2Date(dateStr: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        return formatter.date(from: dateStr)!
    }
    
    static func timeInterval2Date(dateVal:Double) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        let date = Date(timeIntervalSince1970: dateVal/1000)
        return date
    }
    
    static func TimeElapsed(seconds: TimeInterval) -> String {
        var sec = seconds
        if(sec < 0){
            sec = 0 - seconds
        }
        var elapsed = String()
        if sec < 60 {
            elapsed = "Just now"
        } else if sec < (60 * 60) {
            let minutes =  Int(sec / 60)
            elapsed = String(format: "%d %@", minutes, (minutes > 1) ? "mins ago" : "min ago")
        } else if sec < ( 24 * 60 * 60 ) {
            let hours = Int(sec / (60 * 60))
            elapsed = String(format: "%d %@", hours, (hours > 1) ? "hours ago" : "hours ago")
        } else {
            let days = Int(sec / (24 * 60 * 60))
            elapsed = String(format: "%d %@", days, (days > 1) ? "days ago" : "days ago")
        }
        return elapsed
    }
}
