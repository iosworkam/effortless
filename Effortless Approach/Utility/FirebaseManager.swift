//
//  FirebaseManager.swift
//  Effortless Approach
//
//  Created by AMISHA BELADIYA on 02/12/17.
//  Copyright © 2017 Aimaccent. All rights reserved.
//

import Foundation
import Firebase
import FirebaseStorage

class FirebaseManager: NSObject {
   
    class func getDatabaseReference() -> DatabaseReference {
            return Database.database().reference()
    }
    class func getStorageReference() -> StorageReference {
        return Storage.storage().reference()
    }
}
class Chat {
    static var groupId = String()
    var view = UIView()
    
    init(_ groupId_: String, view view_: UIView) {
        Chat.groupId = groupId_
        view = view_
    }
    
    class func sendMsg(text:String,chatType:String,fromdUserId:String) {
        let chatRef = FirebaseManager.getDatabaseReference().child("messages").child(Common.instance.GetDeviceId()).child(fromdUserId).childByAutoId()
        if chatType == "text" {
            let item = ["from": Common.instance.GetDeviceId(),
                        "time": ServerValue.timestamp(),
                        "seen": false,
                        "type": chatType,
                        "message": text] as [String : Any]
            
            chatRef.setValue(item) { (error, ref) in
                if error != nil {
                    debugPrint("Outgoing sendMessage network error.")
                } else {
                    print("Suucesfullty send")
                }
            }
            
        }
        let chatRef2 = FirebaseManager.getDatabaseReference().child("messages").child(fromdUserId).child(Common.instance.GetDeviceId()).childByAutoId()
        if chatType == "text" {
            let item = ["from": Common.instance.GetDeviceId(),
                        "time": ServerValue.timestamp(),
                        "seen": false,
                        "type": chatType,
                        "message": text] as [String : Any]
            
            chatRef2.setValue(item) { (error, ref) in
                if error != nil {
                    debugPrint("Outgoing sendMessage network error.")
                } else {
                    print("Suucesfullty send")
                }
            }
            
        }
    }
    }

class UserInfo: NSObject{
    class func logOutUser(completion: @escaping (Bool) -> Swift.Void) {
        if Auth.auth().currentUser != nil {
           
        do {
            try Auth.auth().signOut()
           
            completion(true)
        } catch let error {
            debugPrint(error.localizedDescription)
            completion(false)
        }
    }
    }
}
class UserClass: NSObject {
  
    var name = String()
    var email = String()
    var id = String()
    var profilePic = UIImage()
    
     let uniqueId = Common.instance.GetDeviceId()
    class func setUserImage(userImg: UIImage,completion: @escaping (Firebase.User) -> Swift.Void,failure: @escaping (Error) -> Swift.Void ) {
         let user = Auth.auth().currentUser
        let user_id = (Auth.auth().currentUser?.uid)!
        let fileName = user_id + ".jpg"
        let storageRef = FirebaseManager.getStorageReference().child("Users").child(fileName)
        let data = UIImageJPEGRepresentation(userImg, 0.5)
        storageRef.putData(data!, metadata: nil) { (metadata, error) in
            if error == nil {
                let changeRequest = user?.createProfileChangeRequest()
                changeRequest?.photoURL = metadata?.downloadURL()
                changeRequest?.commitChanges() { (error) in
                    if error == nil {
                        let urlString = metadata?.downloadURL()?.absoluteString
                        let uniqueId = Common.instance.GetDeviceId()
                      
                        
                        let postdata:[String : String] = ["image":urlString!]
                       
                        let userDBRef = FirebaseManager.getDatabaseReference().child("Users").child(uniqueId)
                   
                        userDBRef.updateChildValues(postdata, withCompletionBlock: { (error, ref) in
                            if error == nil {
                                completion(user!)
                            } else {
                                failure(error!)
                            }
                        })
                    } else {
                        failure(error!)
                    }
                }
            }else {
                failure(error!)
            }
        }
        
    }
    

}
