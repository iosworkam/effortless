//
//  UserCredentialSettingview.swift
//  Effortless Approach
//
//  Created by AMISHA BELADIYA on 17/12/17.
//  Copyright © 2017 Aimaccent. All rights reserved.
//
import Firebase
import FirebaseDatabase
import UIKit
import SkyFloatingLabelTextField
class UserCredentialSettingview: UIViewController {

    @IBOutlet weak var ChangeBtn: UIButton!
    @IBOutlet weak var textFiled: SkyFloatingLabelTextField!
    @IBOutlet weak var edtingViewHeightconstarin: NSLayoutConstraint!
    var selectedbtn : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        
         ChangeBtn.isHidden = true
         textFiled.isHidden = true
         edtingViewHeightconstarin.constant = 0
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func chanegMail(_ sender: Any) {
       self.CheakConstrain()
        
        textFiled.text = ""
        textFiled.placeholder = "New email"
        ChangeBtn.setTitle("CHANGE", for: .normal)
        selectedbtn = 1
         self.ChangeDataToServer()
    }
    @IBAction func chanegpassword(_ sender: Any) {
          textFiled.text = ""
        textFiled.placeholder = "New Password"
        ChangeBtn.setTitle("CHANGE", for: .normal)
        self.CheakConstrain()
        selectedbtn = 2
        
    }
    @IBAction func sendpasswordresetmail(_ sender: Any) {
          textFiled.text = ""
        textFiled.placeholder = "Enter Email"
        ChangeBtn.setTitle("SEND", for: .normal)
        selectedbtn = 3
    }
    @IBAction func removeUser(_ sender: Any) {
        
        Auth.auth().currentUser?.delete(completion:  {(_ error: Error?) -> Void in
            if error != nil {
                // An error happened.
            }
            else {
                // Email updated.
            }
        })

    }
    @IBAction func changeBtnAction(_ sender: Any) {
          self.ChangeDataToServer()
    }
   
  
    func ChangeDataToServer() {
        
        if(selectedbtn == 2){
            
            Auth.auth().currentUser?.updatePassword(to: textFiled.text!, completion: {(_ error: Error?) -> Void in
                if error != nil {
                // An error happened.
                }
                else {
                // Password updated.
                    self.navigationController?.popToRootViewController(animated: true)
                }
                })

        }
        else if(selectedbtn == 1){
          
            Auth.auth().currentUser?.updateEmail(to: textFiled.text!, completion:  {(_ error: Error?) -> Void in
                if error != nil {
                    // An error happened.
                }
                else {
                   self.navigationController?.popToRootViewController(animated: true)
                }
            })
        }
        else if(selectedbtn == 4){
            
            
          
           
        }
        else if(selectedbtn == 3){
            Auth.auth().sendPasswordReset(withEmail: textFiled.text!) { error in
                if error != nil {
                    // An error happened.
                }
                else {
                   self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
    }
    @IBAction func signout(_ sender: Any) {
        UserInfo.logOutUser(completion: { (bool) in
            if bool {
                UserDefaults.standard.removeObject(forKey: "isRegister")
                let rootVC  = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginbaseView")
                let navigation = UINavigationController(rootViewController: rootVC)
                let app = UIApplication.shared.delegate as! AppDelegate
                app.window?.rootViewController = navigation
            }
        })
    }
    func CheakConstrain(){
        view.layoutIfNeeded()
        UIView.animate(withDuration: 0.6, animations: { () -> Void in
            if(self.edtingViewHeightconstarin.constant == 0){
                self.edtingViewHeightconstarin.constant = 100
                self.ChangeBtn.isHidden = false
                self.textFiled.isHidden = false
                self.view.layoutIfNeeded()
            }
          
        })
   
   }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
