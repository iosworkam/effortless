//
//  ChangeStatusView.swift
//  Effortless Approach
//
//  Created by AMISHA BELADIYA on 02/12/17.
//  Copyright © 2017 Aimaccent. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
class ChangeStatusView: UIViewController {
    
@IBOutlet weak var statustext: SkyFloatingLabelTextField!
    
    var staustext : String?
    @IBAction func savechanegAction(_ sender: Any) {
        
        HUD.show(to: self.view)
         let uniqueId = Common.instance.GetDeviceId()
        
        let postdata:[String : String] = [
            "status":statustext.text!]
        
        let userDBRef = FirebaseManager.getDatabaseReference().child("Users").child(uniqueId)
       
        userDBRef.updateChildValues(postdata, withCompletionBlock: { (error, ref) in
            if let error_code = error {
                debugPrint(error_code.localizedDescription)
                HUD.hide(to: self.view)
                
            } else  {
                HUD.hide(to: self.view)
                self.navigationController?.popViewController(animated: true)
                
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
      statustext.text = staustext
        statustext.becomeFirstResponder()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
