//
//  AllUserListview.swift
//  Effortless Approach
//
//  Created by AMISHA BELADIYA on 03/12/17.
//  Copyright © 2017 Aimaccent. All rights reserved.
//

import UIKit

class AllUserListview: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var usertableview: UITableView!
    var Alldata = [[String:AnyObject]]()
    var type = String()
    var UserDataKey = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getdata()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getdata(){
        HUD.show(to: self.view)
        let userRef = FirebaseManager.getDatabaseReference().child("Users")
        userRef.observeSingleEvent(of: .value, with: { (snapshot) in
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            for dic in postDict{
                let dickeyval = dic.value as! [String : AnyObject]
                self.Alldata.append(dickeyval)
                   self.UserDataKey.append(dic.key)
            }
               HUD.hide(to: self.view)
            self.usertableview.reloadData()
        }) { (error) in
            
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Alldata.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell :allusercell = tableView.dequeueReusableCell(withIdentifier: "allusercell", for: indexPath) as! allusercell
        cell.userimg?.setRadius()
        let userdatasingle = self.Alldata[indexPath.row]
        if let usernam = userdatasingle["name"]  as? String{
        cell.usernam.text = usernam
        }
        if let statuss = userdatasingle["status"] as? String{
        cell.status.text = statuss
        }
        if let image = userdatasingle["image"] as? String{
            cell.userimg.kf.setImage(with: URL(string: image))
        }
        if let image = userdatasingle["image"] as? String{
            
            cell.userimg.kf.setImage(with: URL(string:image))
            if((userdatasingle["image"] as! String) == "default" || String(describing: userdatasingle["image"]).isEmpty == true){
                cell.userimg.image = UIImage(named:"person-placeholder")
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(type == "UserSetting"){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "RequestViewController") as? RequestViewController
             vc?.CuurntUserId = (UserDataKey[indexPath.row] as? String)!
            self.navigationController?.pushViewController(vc!, animated: true)
        
        }
        else{
            let alert = UIAlertController(title:nil, message: "Select Option", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Open Profile", style: UIAlertActionStyle.default, handler: { action in
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "RequestViewController") as? RequestViewController
               
                self.navigationController?.pushViewController(vc!, animated: true)
                
                
            }))
            alert.addAction(UIAlertAction(title: "Send message", style: UIAlertActionStyle.default, handler: { action in
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "RequestViewController") as? RequestViewController
               // vc?.CuurntUserId = (UserDataKey[indexPath.row] as? String)!
                self.navigationController?.pushViewController(vc!, animated: true)
                
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive,  handler: { action in
                
             
                
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95.0
    }
    
}

