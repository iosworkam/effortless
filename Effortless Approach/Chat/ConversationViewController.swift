//
//  ConversationViewController.swift
//  Effortless Approach
//
//  Created by AMISHA BELADIYA on 06/12/17.
//  Copyright © 2017 Aimaccent. All rights reserved.
//

import UIKit
import Firebase
class ConversationViewController: UIViewController ,UITextViewDelegate,UITableViewDataSource,UITableViewDelegate{
    
    var user_info = [String:Any]()
    var FromSendId = String()
    var messages = [messageStruct]()
    @IBOutlet weak var chatTblView: UITableView!
    @IBOutlet weak var inputTextView: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    var chatQuery = DatabaseQuery()
    var initalized = Bool()
    // title nd status
    let ShormesgArry = ["Can I bring you the coffee?",
    "Hi, you look nice today, can we friends?",
    "Can i help you?",
    "You are amazing :). Can we meet today?",
    "Hello, How are you?",
    "Do you like Pizza? can i buy for you?",
    "Can we have drinks together?",
    "I like you, How are you today?",
    "In search of good friends, can we?",
    "Can we go to movie together?"]
    @IBOutlet weak var shortmsgview: UIView!
    @IBOutlet weak var shormsgtableview: UITableView!
    var navview = NaviagtionUi()
    @IBOutlet weak var constraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addObserver()
        setOnlineStatus(status: true)
        navview = NaviagtionUi.instanceFromNib()
        navview.frame = CGRect.init(x: 35, y: 0, width: UIScreen.main.bounds.width - 35, height: 40)
        self.navigationController?.navigationBar.addSubview(navview)
       shormsgtableview.isHidden = true
       shortmsgview.isHidden = true
        self.view.layoutIfNeeded()
        navview.userProfileimg.image = UIImage(named: "person")
        navview.userProfileimg.setRadius()

      
      
        inputTextView.delegate = self
        chatTblView.rowHeight = UITableViewAutomaticDimension
        chatTblView.estimatedRowHeight = 45
        chatTblView.tableFooterView = UIView(frame: .zero)
        
        
        
        let sendImg = UIImage(named: "send")?.withRenderingMode(.alwaysTemplate)
        sendButton.setImage(sendImg, for: .normal)
        
     
        
        messages = [messageStruct]()
        setProfileButton()
        chatQuery = FirebaseManager.getDatabaseReference().child("messages").child(Common.instance.GetDeviceId()).child(FromSendId)   //.queryOrdered(byChild: "date").queryStarting(atValue: Date().millisecondsSince1970)
        chatQuery.keepSynced(true)
        loadMessages()
        
        self.hideKeyboardWhenTappedAround()
        self.dismissKeyboard()
        
      setOnlineOrnot()
        // Do any additional setup after loading the view.
    }
    @IBAction func getShormesg(_ sender: Any) {
        if(shormsgtableview.isHidden == true){
            shormsgtableview.isHidden = false
            shortmsgview.isHidden = false
        }
            
        else{
            shormsgtableview.isHidden = true
            shortmsgview.isHidden = true
        }
    }
    func setOnlineStatus(status:Bool){
        let uniqueId = Common.instance.GetDeviceId()
        print("Your device identifires =>\(String(describing: uniqueId))")
        
        let postdata:[String : AnyObject] = [
            "status":status as AnyObject,
            "timestamp":ServerValue.timestamp() as AnyObject]
        
     //    let userDBRef = FirebaseManager.getDatabaseReference().child("Chat").child(uniqueId).child(FromSendId)
         let userDBRef2 = FirebaseManager.getDatabaseReference().child("Chat").child(FromSendId).child(uniqueId)
       
//        userDBRef.setValue(postdata, withCompletionBlock: { (error, ref) in
//            if let error_code = error {
//                debugPrint(error_code.localizedDescription)
//                HUD.hide(to: self.view)
//            } else  {
//
//                HUD.hide(to: self.view)
//
//            }
//        })
        userDBRef2.setValue(postdata, withCompletionBlock: { (error, ref) in
            if let error_code = error {
                debugPrint(error_code.localizedDescription)
                HUD.hide(to: self.view)
            } else  {
                
                HUD.hide(to: self.view)
                
            }
        })
        
    }
    func setProfileButton()  {
        let ref = FirebaseManager.getDatabaseReference().child("Users").child(FromSendId)
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.hasChildren() {
                let userDetail = snapshot.value as! [String:Any]
                self.navview.Username?.text = userDetail["name"] as? String
              
                if userDetail["image"] as? String != "default"{
                    let userImageUrlString = userDetail["image"] as? String
                    self.navview.userProfileimg.kf.setImage(with: URL(string: userImageUrlString!), placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, type, url) in
                       
                    })
                }
            }
        }) { (error) in
            debugPrint(error.localizedDescription)
        }
    }
    func setOnlineOrnot()  {
        let ref = FirebaseManager.getDatabaseReference().child("Chat").child(Common.instance.GetDeviceId()).child(FromSendId)
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.hasChildren() {
                let userDetail = snapshot.value as! [String:Any]
                let statusTime = userDetail["timestamp"] as! Double
            
                let timeInterval = Double(statusTime/1000)
                let date = NSDate(timeIntervalSince1970: timeInterval)
                let deltaSeconds = date.timeIntervalSince(Date())
                self.navview.status?.text = "last seen " + Date.TimeElapsed(seconds: deltaSeconds)  // Ex- 1 hour ago
            }
        }) { (error) in
            debugPrint(error.localizedDescription)
        }
    }
    //MARK:- Load messages
    func loadMessages() {
      ///  HUD.show(to: self.view)
        initalized = false
        chatQuery.observe(.childAdded, with: { (snapshot) in
            
            if self.initalized {
                let message = self.addObject(snapShot: snapshot.value as! [String : Any])
                self.messages.append(message)
                self.chatTblView.beginUpdates()
                
                let indexpath = IndexPath(item: self.messages.count-1, section: 0)
                self.chatTblView.insertRows(at: [indexpath], with: .automatic)
                
                self.chatTblView.endUpdates()
                self.scrollTableViewToBottom()
            } else {
                let message = self.addObject(snapShot: snapshot.value as! [String : Any])
                self.messages.append(message)
                self.chatTblView.reloadData()
                self.scrollTableViewToBottom()
            }
           
        }) { (error) in
            debugPrint(error.localizedDescription)
            HUD.hide(to: self.view)
        }
        chatQuery.observeSingleEvent(of: .value, with: { (snapshot) in
            self.initalized = true
            HUD.hide(to: self.view)
        }) { (error) in
            debugPrint(error.localizedDescription)
            HUD.hide(to: self.view)
        }
    }
    func addObject(snapShot: [String:Any]) -> messageStruct {
        var message = messageStruct()
        message.type = snapShot["type"] as! String
        if let date = snapShot["date"] as? Double{
        message.time = date
        }
        else{
             message.time = snapShot["time"] as! Double
        }
        message.message = snapShot["message"] as! String
        message.seen = snapShot["seen"] as! Bool
        message.from = snapShot["from"] as! String
        return message
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
        navview.removeFromSuperview()
         setOnlineStatus(status: false)
    }
    //MARK:Keybourd Notification
    func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    @objc func keyboardWillShow(_ notification: Notification) {
        let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height
        constraint.constant = keyboardHeight
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        scrollTableViewToBottom()
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        constraint.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
   

    //MARK:- Tableview Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView.tag == 200){
        return messages.count
        }
        else{
            return ShormesgArry.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
          if(tableView.tag == 200){
        let message = messages[indexPath.row]
        var bubbleView = UIImageView()
         var timelabel = UILabel()
        bubbleView.backgroundColor = UIColor.blue
        var cell = UITableViewCell()
     
        if message.type == "text" {
            if message.from != Common.instance.GetDeviceId() {
                cell = tableView.dequeueReusableCell(withIdentifier: "RightCell", for: indexPath)
                bubbleView = cell.contentView.viewWithTag(10) as! UIImageView
                bubbleView.image = UIImage(named: "bubble_right")?.resizableImage(withCapInsets: UIEdgeInsets(top: 8.0, left: 8.0, bottom: 16.0, right: 20.0)).withRenderingMode(.alwaysTemplate)
                 timelabel = cell.contentView.viewWithTag(18)  as! UILabel
                bubbleView.tintColor = UIColor.white
                cell.detailTextLabel?.textColor =  UIColor.black
              
                //time
                timelabel = cell.contentView.viewWithTag(18)  as! UILabel
                let statusTime = message.time
                let timeInterval = Double(statusTime/1000)
                let date = Date.init(timeIntervalSince1970: timeInterval)
                
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = NSTimeZone.local
                dateFormatter.dateFormat = "hh:mm"
                let time = dateFormatter.string(from: date)
                timelabel.text = time
                
             
                
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "LeftCell", for: indexPath)
                bubbleView = cell.contentView.viewWithTag(10)  as! UIImageView
                bubbleView.image = UIImage(named: "bubble_left")?.resizableImage(withCapInsets: UIEdgeInsets(top: 8.0, left: 20.0, bottom: 16.0, right: 8.0)).withRenderingMode(.alwaysTemplate)
               
                cell.detailTextLabel?.textColor =  UIColor.white
                bubbleView.tintColor = UIColor(red: 18/255, green: 140/255, blue: 126/255, alpha: 0.5)

                
                //time
                timelabel = cell.contentView.viewWithTag(18)  as! UILabel
                let statusTime = message.time
                let timeInterval = Double(statusTime/1000)
                let date = Date.init(timeIntervalSince1970: timeInterval)
                
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = NSTimeZone.local
                dateFormatter.dateFormat = "hh:mm"
                let time = dateFormatter.string(from: date)
                timelabel.text = time
             
            }
            let lblText = cell.contentView.viewWithTag(20) as! UILabel
            lblText.text = message.message
            //lblText.textColor = UIColor.black
        }
        return cell
            
        }
          else{
               var cell = ShormsgCell()
              cell = tableView.dequeueReusableCell(withIdentifier: "ShormsgCell", for: indexPath) as! ShormsgCell
              cell.msgtext.text = ShormesgArry[indexPath.row]
              return cell
        }
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if(tableView.tag == 200){
        tableView.deselectRow(at: indexPath, animated: false)
        }
         else{
            UIPasteboard.general.string = ShormesgArry[indexPath.row]
            if let myString = UIPasteboard.general.string {
                inputTextView.text = myString
            }
            
        }
    }
    //MARK: Send Message
    @IBAction func sendBtnPressed(_ sender: Any) {
        let sendTxt = inputTextView.text?.trimmingCharacters(in: .whitespaces).trimmingCharacters(in: .whitespacesAndNewlines)
        if sendTxt?.characters.count != 0 {
            // self.setProfileButton()
            self.sendMessageWithText(sendTxt!)
        }
        
    }
    
    
    func sendMessageWithText(_ sendText:String)  {
        Chat.sendMsg(text: sendText, chatType: "text",fromdUserId: FromSendId)
        inputTextView.text = ""
       
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    func scrollTableViewToBottom() {
        if messages.count > 0 {
            if messages.count-1 == 0 {
            } else {
                let indexPath = IndexPath(row: self.messages.count-1, section: 0)
                self.chatTblView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        chatTblView.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    @objc func goUserPrfile() {
        
    }
    
    
}
class NaviagtionUi: UIView {
    
    @IBOutlet  var status: UILabel?
    @IBOutlet  var Username: UILabel?
 
    @IBOutlet weak var userProfileimg: UIImageView!
    
    // MARK:- Delegate
    
    class func instanceFromNib() -> NaviagtionUi {
        let myClassNib = UINib(nibName: "NavView", bundle: nil)
        return myClassNib.instantiate(withOwner: nil, options: nil)[0] as! NaviagtionUi
    }
}

