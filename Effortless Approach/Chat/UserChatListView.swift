//
//  UserChatListView.swift
//  Effortless Approach
//
//  Created by AMISHA BELADIYA on 06/12/17.
//  Copyright © 2017 Aimaccent. All rights reserved.
//

import UIKit
import PopOverMenu
class UserChatListView: UIViewController,UITableViewDelegate,UITableViewDataSource,UIAdaptivePresentationControllerDelegate {

    @IBOutlet weak var nouserlistView: UIView!
    @IBOutlet weak var tableview: UITableView!
    var Alldata = [[String:AnyObject]]()
    var type = String()
    var UserDataKey = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
          self.getdata()
        self.navigationItem.title = "EFFORTLESS"
        setupnavigation()
        // Do any additional setup after loading the view.
    }
    
    
    func setupnavigation(){
        let dormenu = UIBarButtonItem(image: UIImage(named: "dots-vertical"), landscapeImagePhone: nil, style: .done, target: self, action: #selector(slidemenu(_:)))
        let search = UIBarButtonItem(image: UIImage(named: "magnify"), landscapeImagePhone: nil, style: .done, target: self, action: #selector(searchaction(_:)))
        self.navigationItem.rightBarButtonItems = [dormenu,search]
    }
    @objc func slidemenu(_ sender: UIBarButtonItem) {
          let titles:NSArray = ["Profile Setting","Account Setting", "All User", "Log out"]
        
        
        let popOverViewController = PopOverViewController.instantiate()
        popOverViewController.setTitles(titles as! Array<String>)
        
        
        // option parameteres
        // popOverViewController.setSelectRow(1)
        // popOverViewController.setShowsVerticalScrollIndicator(true)
        // popOverViewController.setSeparatorStyle(UITableViewCellSeparatorStyle.singleLine)
        
        popOverViewController.popoverPresentationController?.barButtonItem = sender
        popOverViewController.preferredContentSize = CGSize(width: 200, height:180)
        popOverViewController.presentationController?.delegate = self
        popOverViewController.completionHandler = { selectRow in
            switch (selectRow) {
            case 0:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as? RegisterViewController
                vc?.ScreenType = "edit"
                self.navigationController?.pushViewController(vc!, animated: true)
                break
            case 1:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileSettingViewController") as? ProfileSettingViewController
                
                self.navigationController?.pushViewController(vc!, animated: true)
                break
            case 2:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AllUserListview") as? AllUserListview
                vc?.type = "UserSetting"
                self.navigationController?.pushViewController(vc!, animated: true)
                break
            case 3:  UserInfo.logOutUser(completion: { (bool) in
                if bool {
                    UserDefaults.standard.removeObject(forKey: "isRegister")
                    let rootVC  = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginbaseView")
                    let navigation = UINavigationController(rootViewController: rootVC)
                    let app = UIApplication.shared.delegate as! AppDelegate
                    app.window?.rootViewController = navigation
                }
            })
                break
            default:
                break
            }
            
        };
        present(popOverViewController, animated: true, completion: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
        
    }

    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    @objc func searchaction(_ sender: UIBarButtonItem) {
        // Implement action
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getdata(){
        HUD.show(to: self.view)
        let userRef = FirebaseManager.getDatabaseReference().child("Friends").child(Common.instance.GetDeviceId())
        userRef.observeSingleEvent(of: .value, with: { (snapshot) in
             HUD.hide(to: self.view)
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            if(postDict.count > 0){
                self.nouserlistView.isHidden = true
                self.tableview.isHidden = false
            for dic in postDict{
                let dickeyval = dic.value as! [String : AnyObject]
                let userRef = FirebaseManager.getDatabaseReference().child("Users").child(dic.key)
                userRef.observeSingleEvent(of: .value, with: { (snapshot) in
                    let postDict = snapshot.value as? [String : AnyObject] ?? [:]
                    print(postDict)
                   self.Alldata.append(postDict)
                      self.UserDataKey.append(dic.key)
                    self.tableview.reloadData()
                }) { (error) in
                    
                }
              
              
            }
            }
            else{
                self.nouserlistView.isHidden = false
                self.tableview.isHidden = true
            }
           
        }) { (error) in
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
       self.navigationItem.title = "EFFORTLESS"
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Alldata.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell :allusercell = tableView.dequeueReusableCell(withIdentifier: "allusercell", for: indexPath) as! allusercell
        cell.userimg?.setRadius()
        let userdatasingle = self.Alldata[indexPath.row]
        
        cell.usernam.text = userdatasingle["name"]  as? String
        cell.status.text = userdatasingle["status"] as? String
        if  let imh = userdatasingle["image"] as? String{
        if(String(describing: userdatasingle["image"]!) == "default" || String(describing: userdatasingle["image"]!) == ""){
            cell.userimg.image = UIImage(named:"person-placeholder")
        }
        else{
            cell.userimg.kf.setImage(with: URL(string: (userdatasingle["image"]! as! String)))
        }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let alert = UIAlertController(title:nil, message: "Select Option", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Open Profile", style: UIAlertActionStyle.default, handler: { action in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "RequestViewController") as? RequestViewController
            vc?.CuurntUserId = (self.UserDataKey[indexPath.row])
            self.navigationController?.pushViewController(vc!, animated: true)
            
            
        }))
        alert.addAction(UIAlertAction(title: "Send message", style: UIAlertActionStyle.default, handler: { action in
            
            let userdatasingle = self.Alldata[indexPath.row]
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConversationViewController") as? ConversationViewController
            vc?.user_info = userdatasingle
            vc?.FromSendId =  (self.UserDataKey[indexPath.row])
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive,  handler: { action in
        
        }))
        self.present(alert, animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95.0
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
